package org.randombits.support.core.env;

/**
 * This component helps discover values that are present in the current execution environment that
 * are often temporary, and have a variety of ways of discovering them. A common example is
 * {@link javax.servlet.http.HttpServletRequest}. Each request to the server has one, but retrieving
 * the current one varies depending on the environment. This assistant provides a central way of
 * discovering values like that by allowing other plugins to register {@link EnvironmentProvider} instances
 * through the &lt;environment-provider&gt; module type.
 *
 * <p>An example of retrieving the current HttpServletRequest is as follows:</p>
 *
 * <pre>
 * HttpServletRequest req = environmentAssistant.getValue( HttpServletRequest.class );
 * </pre>
 *
 * The actual execution will poll through the registered {@link EnvironmentProvider} instances until one
 * returns a value.
 */
public interface EnvironmentAssistant {

    /**
     * Retrieves the current value of the specified type, if it is available.
     *
     * @param type The type of class to retrieve.
     * @return The current value, or <code>null</code> if none is available.
     */
    public <T> T getValue( Class<T> type );
}
