package org.randombits.support.core.env.servlet;

import org.randombits.support.core.env.AbstractEnvironmentProvider;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Provides support for getting HTTP/Servlet related classes, such as
 * HttpServletRequest/Response, HttpSession, etc.
 */
public abstract class AbstractServletProvider extends AbstractEnvironmentProvider {

    /**
     * This constructor sets the default set of supported classes to {@link HttpServletRequest},
     * {@link HttpServletResponse}, {@link HttpSession}, {@link ServletConfig} and {@link ServletContext}.
     * Subclasses which only return a subset (or a super-set) may wish to call the {@link #AbstractServletProvider(Class[])}
     * constructor instead.
     */
    protected AbstractServletProvider() {
        super( HttpServletRequest.class, HttpServletResponse.class, HttpSession.class, ServletConfig.class, ServletContext.class );
    }

    protected AbstractServletProvider( Class<?>... supportedTypes ) {
        super( supportedTypes );
    }

    @Override
    public <T> T getValue( Class<T> type ) {
        if ( HttpServletRequest.class.equals( type ) ) {
            return type.cast( getHttpServletRequest() );
        } else if ( HttpServletResponse.class.equals( type ) ) {
            return type.cast( getHttpServletResponse() );
        } else if ( HttpSession.class.equals( type ) ) {
            return type.cast( getHttpSession() );
        } else if ( ServletConfig.class.equals( type ) ) {
            return type.cast( getServletConfig() );
        } else if ( ServletContext.class.equals( type ) ) {
            return type.cast( getServletContext() );
        }
        return null;
    }

    /**
     * @return the current {@link HttpSession}, if the current {@link HttpServletRequest} is available.
     */
    protected HttpSession getHttpSession() {
        HttpServletRequest req = getHttpServletRequest();
        return ( req != null ) ? req.getSession() : null;
    }

    /**
     * @return the current {@link ServletContext}, if the current {@link HttpServletRequest} is available;
     */
    protected ServletContext getServletContext() {
        HttpSession session = getHttpSession();
        return ( session != null ) ? session.getServletContext() : null;
    }

    /**
     * @return the current {@link ServletConfig}.
     */
    protected abstract ServletConfig getServletConfig();

    /**
     * @return the current {@link HttpServletResponse}.
     */
    protected abstract HttpServletResponse getHttpServletResponse();

    /**
     * @return the current {@link HttpServletRequest}.
     */
    protected abstract HttpServletRequest getHttpServletRequest();
}
