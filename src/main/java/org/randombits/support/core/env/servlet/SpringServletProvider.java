package org.randombits.support.core.env.servlet;

import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Returns Servlet-related objects from the Spring context.
 */
public class SpringServletProvider extends AbstractServletProvider {

    public SpringServletProvider() {
        super( HttpServletRequest.class, HttpSession.class, ServletContext.class );
    }

    @Override
    protected ServletConfig getServletConfig() {
        return null;
    }

    @Override
    protected HttpServletResponse getHttpServletResponse() {
        return null;
    }

    @Override
    protected HttpServletRequest getHttpServletRequest() {
        try {
            return getServletRequestAttributes().getRequest();
        } catch ( IllegalStateException e ) {
            return null;
        }
    }

    private ServletRequestAttributes getServletRequestAttributes() {
        return ( (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes() );
    }

}
