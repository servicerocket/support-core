package org.randombits.support.core.env.general;

import com.atlassian.sal.api.timezone.TimeZoneManager;
import org.randombits.support.core.env.SimpleEnvironmentProvider;

import java.util.TimeZone;

/**
 * Provides the default {@link TimeZone} for the current user.
 */
public class TimeZoneProvider extends SimpleEnvironmentProvider<TimeZone> {

    private final TimeZoneManager timeZoneManager;

    public TimeZoneProvider( TimeZoneManager timeZoneManager ) {
        this.timeZoneManager = timeZoneManager;
    }

    @Override
    public TimeZone getValue() {
        TimeZone zone;

        zone = timeZoneManager.getUserTimeZone();
        if ( zone == null )
            zone = timeZoneManager.getDefaultTimeZone();
        if ( zone == null )
            zone = TimeZone.getDefault();

        return zone;
    }
}
