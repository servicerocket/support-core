package org.randombits.support.core.env;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/**
 * A base implementation for {@link EnvironmentProvider} instances.
 */
public abstract class AbstractEnvironmentProvider implements EnvironmentProvider {

    private final Set<Class<?>> supportedTypes;

    private int weight;

    public AbstractEnvironmentProvider( Class<?>... supportedTypes ) {
        this.supportedTypes = Collections.unmodifiableSet( new HashSet<Class<?>>( Arrays.asList( supportedTypes ) ) );
    }

    public void setWeight( int weight ) {
        this.weight = weight;
    }

    @Override
    public int getWeight() {
        return weight;
    }

    @Override
    public Set<Class<?>> getSupportedTypes() {
        return supportedTypes;
    }
}
