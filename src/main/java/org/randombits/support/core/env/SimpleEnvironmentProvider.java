package org.randombits.support.core.env;

import org.randombits.utils.lang.ClassUtils;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/**
 * This is an abstract base class for simple, single-type environment providers. Simply
 * define the type supported and implement {@link #getValue()}.
 */
public abstract class SimpleEnvironmentProvider<S> implements EnvironmentProvider {

    private final Class<S> supportedType;

    private final Set<Class<?>> supportedTypes;

    @SuppressWarnings({"unchecked"})
    public SimpleEnvironmentProvider() {
        supportedType = (Class<S>) ClassUtils.getTypeArguments( SimpleEnvironmentProvider.class, getClass() ).get( 0 );
        supportedTypes = Collections.unmodifiableSet( new HashSet<Class<?>>( Arrays.asList( supportedType ) ) );
    }

    protected Class<?> getSupportedType() {
        return supportedType;
    }

    @Override
    public int getWeight() {
        return 0;
    }

    @Override
    public Set<Class<?>> getSupportedTypes() {
        return supportedTypes;
    }

    /**
     * Checks if the type matches the {@link #getSupportedType()} value, and if so,
     * calls {@link #getValue()}.
     *
     * @param type The type of value to return.
     * @param <T>  The type.
     * @return The value, if the type matches and a value is available. Otherwise, <code>null</code>.
     */
    @Override
    public <T> T getValue( Class<T> type ) {
        if ( supportedType.equals( type ) )
            return type.cast( getValue() );
        return null;
    }

    /**
     * Returns the environment value being provided. This method is only
     * called if the specified type was requested.
     *
     * @return The value.
     */
    protected abstract S getValue();
}
