package org.randombits.support.core.env.general;

import com.atlassian.sal.api.message.LocaleResolver;
import org.randombits.support.core.env.SimpleEnvironmentProvider;

import java.util.Locale;

/**
 * Finds the current {@link Locale} for the specified scope.
 */
public class LocaleProvider extends SimpleEnvironmentProvider<Locale> {

    private final LocaleResolver localeResolver;

    public LocaleProvider( LocaleResolver localeResolver ) {
        this.localeResolver = localeResolver;
    }

    @Override
    public Locale getValue() {
        Locale locale;

        locale = localeResolver.getLocale();
        if ( locale == null )
            locale = Locale.getDefault();

        return locale;
    }
}
