package org.randombits.support.core.env.atlassian;

import com.atlassian.sal.api.user.UserManager;
import com.atlassian.sal.api.user.UserProfile;
import org.randombits.support.core.env.SimpleEnvironmentProvider;

/**
 * Provides the {@link UserProfile} for the currently-logged-in user.
 */
public class UserProfileProvider extends SimpleEnvironmentProvider<UserProfile> {

    private final UserManager userManager;

    public UserProfileProvider( UserManager userManager ) {
        this.userManager = userManager;
    }

    @Override
    protected UserProfile getValue() {
        return userManager.getUserProfile( userManager.getRemoteUsername() );
    }
}
