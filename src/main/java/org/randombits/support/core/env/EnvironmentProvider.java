package org.randombits.support.core.env;

import java.util.Set;

/**
 * Provides an implementation that can retrieve values of a particular type.
 */
public interface EnvironmentProvider {

    /**
     * A provider can give itself a higher priority if it wishes. This should be used judiciously -
     * basically just aim to get higher than another known provider which may be competing. Allow
     * other providers space to give a higher weight also - don't use Integer.MAX_VALUE.
     *
     * @return the weight for this provider.
     */
    int getWeight();

    /**
     * The value for the given type, or <code>null</code> if not supported, or not available.
     *
     * @param type The type of value to return.
     * @param <T>  The type.
     * @return The current value, or <code>null</code>.
     */
    <T> T getValue( Class<T> type );

    /**
     * The set of types that are supported by this provider. The set will be unmodifiable, so
     * if you wish to change the contents, copy it into a new set first.
     *
     * @return the supported type set.
     */
    Set<Class<?>> getSupportedTypes();
}
