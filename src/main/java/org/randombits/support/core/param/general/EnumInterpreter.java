package org.randombits.support.core.param.general;

import org.randombits.support.core.param.InterpretationException;
import org.randombits.support.core.param.ParameterContext;
import org.randombits.support.core.param.ParameterInterpreter;

/**
 * Interprets values as {@link Enum}. By default, it will simply convert the String to upper-case,
 * and then try to access it via {@link Enum#valueOf(Class, String)}. This can be customised
 * using the {@link EnumValues} annotation on methods returning an Enum. For example:
 * <p/>
 * <pre><code>
 *     public enum MyEnum {
 *         FOO, BAR, FOO_BAR
 *     }
 * <p/>
 *     ...
 *     public interface MyParameters extends Parameters {}
 *         \@EnumValues({\@Alias(value="FOO_BAR", aliases="FOOBAR"})
 *         MyEnum getMyValue();
 *     }
 * </code></pre>
 * <p/>
 * This will allow any variation of 'FOOBAR' and 'FOO_BAR' to be understood as MyEnum.FOO_BAR.
 * <p/>
 * <p>Enum values are automatically supported, so there is no need for an \@Interpreter annotation.</p>
 */
public class EnumInterpreter implements ParameterInterpreter<Enum> {

    @Override
    public Class<Enum> getTargetType() {
        return Enum.class;
    }

    @Override
    public <R> R interpret( String inputValue, ParameterContext context, Class<R> returnType ) throws InterpretationException {
        if ( returnType.isEnum() ) {
            EnumValues values = context.getAnnotation( EnumValues.class );
            boolean toUppercase = values != null ? values.toUppercase() : true;

            if ( values != null ) {
                inputValue = findAlias( inputValue, values );
            }

            if ( toUppercase )
                inputValue = inputValue.toUpperCase();

            return asEnumInstance( returnType, inputValue );
        } else {
            throw new InterpretationException( "The parameter return type must be a specific type of Enum, but was '" + returnType.getName() + "'" );
        }
    }

    private <R> R asEnumInstance( Class<R> type, String name ) throws InterpretationException {
        @SuppressWarnings("unchecked")
        Class<? extends Enum> enumType = (Class<? extends Enum>) type;

        try {
            return type.cast( Enum.valueOf( enumType, name ) );
        } catch ( IllegalArgumentException e ) {
            throw new InterpretationException( e.getMessage(), e );
        }
    }

    private String findAlias( String inputValue, EnumValues values ) {
        for ( Alias aliases : values.value() ) {
            for ( String alias : aliases.aliases() ) {
                if ( values.toUppercase() && inputValue.equalsIgnoreCase( alias ) || inputValue.equals( alias ) )
                    return aliases.value();
            }
        }
        return inputValue;
    }
}
