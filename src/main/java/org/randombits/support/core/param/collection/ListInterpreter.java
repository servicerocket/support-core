package org.randombits.support.core.param.collection;

import org.randombits.support.core.param.ParameterAssistant;
import org.randombits.support.core.param.ParameterContext;

import java.util.ArrayList;
import java.util.List;

/**
 * Interprets a parameter as a {@link List} of values.
 * <p/>
 * If the parameter also has the {@link ListValues} annotation, the interpreter defined there will determine
 * the value type and the {@link org.randombits.support.core.param.ParameterInterpreter} used to interpret each
 * individual value. The default is {@link String} and
 * {@link org.randombits.support.core.param.general.StringInterpreter}, respectively.
 * <p/>
 * For example, this will parse a comma-separated list of values as a {@link List} of {@link Double} values.
 * <p/>
 * <pre><code>
 * \@ListValues(value=Double.class, interpreter=DoubleInterpreter.class)
 * List&lt;Double> getNumbers();
 * </code></pre>
 * <p/>
 * Because generic values lose type information at runtime, there is no way to check if the values defined by \@ListValues
 * actually match the value returned by the parameter, so it is up to the user to ensure it matches.
 */
public class ListInterpreter extends AbstractCollectionInterpreter<List<?>> {

    public ListInterpreter( ParameterAssistant parameterAssistant ) {
        super( parameterAssistant );
    }

    @Override
    protected Config createConfig( ParameterContext context ) {
        ListValues listValues = context.getAnnotation( ListValues.class );
        if ( listValues != null ) {
            return new Config( listValues.separator(), listValues.value(), listValues.interpreter(), listValues.listType() );
        } else {
            return new Config( ArrayList.class );
        }
    }
}
