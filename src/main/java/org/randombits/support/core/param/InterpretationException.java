package org.randombits.support.core.param;

/**
 * Thrown when there is a problem during parameter interpretation.
 *
 * @see ParameterInterpreter#interpret(String, ParameterContext)
 */
public class InterpretationException extends Exception {

    public InterpretationException() {
    }

    public InterpretationException( String s ) {
        super( s );
    }

    public InterpretationException( String s, Throwable throwable ) {
        super( s, throwable );
    }

    public InterpretationException( Throwable throwable ) {
        super( throwable );
    }
}
