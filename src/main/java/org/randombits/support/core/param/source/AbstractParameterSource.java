package org.randombits.support.core.param.source;

import org.randombits.support.core.param.DefaultParameterContext;
import org.randombits.support.core.param.ParameterContext;
import org.randombits.support.core.param.ParameterSource;

/**
 * Abstract base class for {@link ParameterSource} implementations. Simply
 * constructs a new {@link DefaultParameterContext} by default.
 */
public abstract class AbstractParameterSource implements ParameterSource {

    /**
     * Constructs a new {@link DefaultParameterContext}.
     *
     * @return the new {@link ParameterContext}
     */
    @Override
    public ParameterContext createContext() {
        return new DefaultParameterContext();
    }
}
