package org.randombits.support.core.param;

/**
 * This is a marker interface for other interfaces that provide access to parameter values.
 *
 *
 */
public interface Parameters {

}
