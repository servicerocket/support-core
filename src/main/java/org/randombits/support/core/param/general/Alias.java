package org.randombits.support.core.param.general;

import java.lang.annotation.*;

/**
 * Represents a set of aliases for an Enum.
 */
@Documented
public @interface Alias {

    /**
     * The name of the enum value.
     *
     * @return The value.
     */
    String value();

    /**
     * @return the list of aliases.
     */
    String[] aliases();
}
