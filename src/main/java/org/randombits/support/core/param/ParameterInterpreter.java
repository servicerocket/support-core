package org.randombits.support.core.param;

/**
 * Implementations of this interface allow for the interpretation of a String value into
 * another object type. It may be a new object, or a reference to an existing object.
 */
public interface ParameterInterpreter<T> {

    /**
     * Returns the target type that this interpreter will convert into.
     *
     * @return The target type.
     */
    Class<T> getTargetType();

    /**
     * Interprets the provided text value, taking into account the <code>format</code>
     * and <code>info</code> if appropriate.
     *
     * @param inputValue The input text value.
     * @param context    The context the value is being interpreted in.
     * @param returnType The return type for the actual parameter.
     * @return The interpreted value.
     * @throws InterpretationException if there is a problem during interpretation.
     */
    <R> R interpret( String inputValue, ParameterContext context, Class<R> returnType ) throws InterpretationException;
}
