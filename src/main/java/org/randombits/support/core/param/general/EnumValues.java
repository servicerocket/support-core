package org.randombits.support.core.param.general;

import org.randombits.support.core.param.Context;

import java.lang.annotation.*;

/**
 * Allows annotation of aliases for {@link Enum} values.
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Context
public @interface EnumValues {

    /**
     * The list of aliases for enum values.
     *
     * @return
     */
    Alias[] value() default {};

    /**
     * If true (the default), values will be converted to uppercase before being discovered.
     *
     * @return <code>true</code> if names are uppercased before being discovered.
     */
    boolean toUppercase() default true;
}
