package org.randombits.support.core.param.collection;

import org.randombits.support.core.param.ParameterAssistant;
import org.randombits.support.core.param.ParameterContext;

import java.util.HashSet;
import java.util.Set;

/**
 * Interprets a parameter String as a {@link Set}.
 * If the parameter also has the {@link SetValues} annotation, the interpreter defined there will determine
 * the value type and the {@link org.randombits.support.core.param.ParameterInterpreter} used to interpret each
 * individual value. The default is {@link String} and
 * {@link org.randombits.support.core.param.general.StringInterpreter}, respectively.
 */
public class SetInterpreter extends AbstractCollectionInterpreter<Set> {

    public SetInterpreter( ParameterAssistant parameterAssistant ) {
        super( parameterAssistant );
    }

    @Override
    protected Config<Set> createConfig( ParameterContext context ) {
        SetValues setValues = context.getAnnotation( SetValues.class );

        if ( setValues != null ) {
            return new Config<Set>( setValues.separator(), setValues.value(), setValues.interpreter(), setValues.implementation() );
        } else {
            return new Config<Set>( HashSet.class );
        }
    }

}
