package org.randombits.support.core.param;

import org.randombits.utils.lang.ClassUtils;

/**
 * Abstract base implementation of {@link ParameterInterpreter}. Subclasses
 * that have a concrete type can simple call the {@link #AbstractParameterInterpreter()}
 * constructor. If the subclass has a generic {@link T} type declared, it must pass
 * a Class to {@link #AbstractParameterInterpreter(Class)}.
 */
public abstract class AbstractParameterInterpreter<T> implements ParameterInterpreter<T> {

    private final Class<T> type;

    @SuppressWarnings("unchecked")
    public AbstractParameterInterpreter() {
        type = (Class<T>) ClassUtils.getTypeArguments( AbstractParameterInterpreter.class, getClass() ).get( 0 );
        assert type != null;
    }

    public AbstractParameterInterpreter( Class<T> type ) {
        this.type = type;
    }

    @Override
    public Class<T> getTargetType() {
        return type;
    }

    /**
     * Interprets the provided text value, taking into account the <code>format</code>
     * and <code>info</code> if appropriate.
     *
     * @param inputValue The input text value.
     * @param context    The context the value is being interpreted in.
     * @param returnType The return type for the actual parameter.
     * @return The interpreted value.
     * @throws InterpretationException if there is a problem during interpretation.
     */
    @Override
    public <R> R interpret( String inputValue, ParameterContext context, Class<R> returnType ) throws InterpretationException {
        if ( type.isAssignableFrom( returnType ) || returnType.isAssignableFrom( type ) ) {
            T value = interpret( inputValue, context );
            if ( returnType.isInstance( value ) ) {
                return returnType.cast( value );
            }
            throw new InterpretationException( "Unable to interpret the value as a '" + type.getName() + "'" );
        }
        throw new InterpretationException( "The interpreter's target type of '" + type.getName() + "' is not compatible with the parameter's return type of '" + returnType.getName() + "'" );
    }

    /**
     * Interprets the provided text value, taking into account the <code>format</code>
     * and <code>info</code> if appropriate.
     *
     * @param inputValue The input text value.
     * @param context    The context the value is being interpreted in.
     * @return The interpreted value.
     * @throws InterpretationException if there is a problem during interpretation.
     */
    public abstract T interpret( String inputValue, ParameterContext context ) throws InterpretationException;
}
