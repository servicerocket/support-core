package org.randombits.support.core.param.general;

import org.randombits.support.core.param.Context;
import org.randombits.support.core.param.collection.AbstractCollectionInterpreter;

import java.lang.annotation.*;

/**
 * Provides custom values for interpreting a value as an {@link java.util.EnumSet}.
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Context
public @interface EnumSetValues {

    /**
     * @return the type of content the Set will contain.
     */
    Class<? extends Enum> value();

    /**
     * The {@link java.util.regex.Pattern} string to use to split values in the set.
     *
     * @return the separator pattern.
     */
    String separator() default AbstractCollectionInterpreter.DEFAULT_SEPARATOR;
}
