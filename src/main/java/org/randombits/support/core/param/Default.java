package org.randombits.support.core.param;

import java.lang.annotation.*;

/**
 * Marks a parameter as providing the default value if the parameter string can't be evaluated.
 */
@Target(ElementType.PARAMETER)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Default {

}
