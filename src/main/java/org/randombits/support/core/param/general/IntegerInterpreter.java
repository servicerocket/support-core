package org.randombits.support.core.param.general;

import org.randombits.support.core.param.AbstractParameterInterpreter;
import org.randombits.support.core.param.InterpretationException;
import org.randombits.support.core.param.ParameterContext;

import java.text.NumberFormat;
import java.text.ParseException;

/**
 * Converts a String into an Integer.
 */
public class IntegerInterpreter extends AbstractParameterInterpreter<Integer> {

    public IntegerInterpreter() {
    }

	@Override
    public Integer interpret( String inputValue, ParameterContext context )
            throws InterpretationException {
        NumberFormat format = context.get( NumberFormat.class, null );
              // Otherwise, try a straight parseInt.
        try {
            try { 
            	return parseInt( inputValue, format );
            } catch(NumberFormatException ex) {
            	float v = parseFloat( inputValue, format ); 
            	if ( Math.abs( v - Math.floor(v) ) <=  0.0000001 ) // accepted as zero in this scenario, otherwise number is not integer 
            		return (int) v;
            	else 
            		throw new InterpretationException( "Unable to parse " + inputValue + " to integer."  );
            }
        } catch ( NumberFormatException e ) {
            throw new InterpretationException( e.getMessage(), e );
        }
    }

    private int parseInt( String value, NumberFormat format ) throws InterpretationException {
        try {
        	if(format != null)
        		return format.parse( value ).intValue();
        	else 
        		return Integer.parseInt( value ); 
        } catch ( ParseException e ) {
            throw new InterpretationException( e.getMessage(), e );
        }
    }
    
    private float parseFloat( String value, NumberFormat format ) throws InterpretationException {
        try {
        	if(format != null)
        		return format.parse( value ).floatValue();
        	else 
        		return Float.parseFloat( value ); 
        } catch ( ParseException e ) {
            throw new InterpretationException( e.getMessage(), e );
        }
    }    
}
