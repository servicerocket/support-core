package org.randombits.support.core.param.collection;

import org.randombits.support.core.param.*;
import org.randombits.support.core.param.general.StringInterpreter;

import java.util.Collection;

/**
 * Abstract base class for {@link Collection} interpreters.
 */
public abstract class AbstractCollectionInterpreter<T extends Collection> extends AbstractParameterInterpreter<T> {

    protected static class Config<T extends Collection> {

        protected final String separator;

        protected final Class<?> valueType;

        protected final Class<? extends ParameterInterpreter<?>> interpreter;

        protected final Class<? extends T> collectionType;

        public Config( Class<? extends T> collectionType ) {
            this( null, null, null, collectionType );
        }

        public Config( String separator, Class<?> valueType, Class<? extends ParameterInterpreter<?>> interpreter, Class<? extends T> collectionType ) {
            this.separator = separator == null ? DEFAULT_SEPARATOR : separator;
            this.valueType = valueType == null ? String.class : valueType;
            this.interpreter = ( valueType == null || valueType == StringInterpreter.class )
                    ? null : interpreter;
            this.collectionType = collectionType;
        }
    }

    public static final String DEFAULT_SEPARATOR = "\\s*,\\s*";

    private final ParameterAssistant parameterAssistant;

    public AbstractCollectionInterpreter( ParameterAssistant parameterAssistant ) {
        this.parameterAssistant = parameterAssistant;
    }

    @Override
    public T interpret( String inputValue, ParameterContext context ) throws InterpretationException {
        Config<T> config = createConfig( context );

        String[] values = inputValue.split( config.separator );
        ParameterInterpreter<?> interpreter = null;
        if ( config.interpreter != null )
            interpreter = parameterAssistant.findParameterInterpreter( config.interpreter );

        return getTargetType().cast( createCollection( values, config.valueType, interpreter, config.collectionType, context ) );
    }

    protected abstract Config<T> createConfig( ParameterContext context ) throws InterpretationException;

    private <V> Collection<V> createCollection( String[] values, Class<V> valueType, ParameterInterpreter<?> valueInterpreter, Class<? extends T> collectionType, ParameterContext context ) throws InterpretationException {
        Collection<V> collection = createInstance( collectionType, valueType );
        for ( String valueString : values ) {
            V value = valueInterpreter.interpret( valueString, context, valueType );
            if ( value != null )
                collection.add( value );
        }

        return collection.size() > 0 ? collection : null;
    }

    public ParameterAssistant getParameterAssistant() {
        return parameterAssistant;
    }

    protected <V> Collection<V> createInstance( Class<? extends T> collectionType, Class<V> valueType ) throws InterpretationException {
        try {
            return ( (Class<? extends Collection<V>>) collectionType ).newInstance();
        } catch ( InstantiationException e ) {
            throw new InterpretationException( e.getMessage(), e );
        } catch ( IllegalAccessException e ) {
            throw new InterpretationException( e.getMessage(), e );
        }
    }

}
