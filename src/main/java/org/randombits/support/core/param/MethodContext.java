package org.randombits.support.core.param;

import java.lang.reflect.Method;

/**
 * Provides context for the method being executed.
 */
public interface MethodContext<ReturnType> {

    /**
     * Returns the instance the method is being executed on.
     *
     * @return The instance.
     */
    Object getInstance();

    /**
     * @return the method being executed.
     */
    Method getMethod();

    /**
     * @return the array of parameters passed in.
     */
    Object[] getMethodParams();

    Class<ReturnType> getReturnType();

    void setReturnValue( ReturnType value );

    ReturnType getReturnValue();

    void setDefaultValue( ReturnType value );

    ReturnType getDefaultValue();
    
    String getInput();
    
    void setInput( String value );

    ParameterContext getParameterContext();
}
