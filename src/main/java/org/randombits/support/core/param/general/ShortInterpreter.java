package org.randombits.support.core.param.general;

import org.randombits.support.core.param.AbstractParameterInterpreter;
import org.randombits.support.core.param.InterpretationException;
import org.randombits.support.core.param.ParameterContext;

import java.text.NumberFormat;
import java.text.ParseException;

/**
 * Converts a String into a {@link Short}.
 */
public class ShortInterpreter extends AbstractParameterInterpreter<Short> {

    public ShortInterpreter() {
    }

    @Override
    public Short interpret( String inputValue, ParameterContext context )
            throws InterpretationException {
        NumberFormat format = context.get( NumberFormat.class, null );
        // First, try the Decimal format.
        if ( format != null )
            return parseShort( inputValue, format );

        // Otherwise, try a straight parseInt.
        try {
            return Short.parseShort( inputValue );
        } catch ( NumberFormatException e ) {
            throw new InterpretationException( e.getMessage(), e );
        }
    }

    private short parseShort( String value, NumberFormat format ) throws InterpretationException {
        try {
            return format.parse( value ).shortValue();
        } catch ( ParseException e ) {
            throw new InterpretationException( e.getMessage(), e );
        }
    }
}
