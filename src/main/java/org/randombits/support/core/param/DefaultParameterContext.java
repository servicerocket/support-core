package org.randombits.support.core.param;

import java.lang.annotation.Annotation;
import java.util.HashMap;
import java.util.Map;

/**
 * Default implementation of {@link ParameterContext}.
 */
public class DefaultParameterContext implements ParameterContext {

    private Map<Class<?>, Object> values;

    private Map<Class<? extends Annotation>, Annotation> annotations;

    public DefaultParameterContext() {
    }

    @Override
    public <A extends Annotation> void setAnnotation( A annotation ) {
        if ( annotations == null )
            annotations = new HashMap<Class<? extends Annotation>, Annotation>();

        annotations.put( annotation.annotationType(), annotation );
    }

    @Override
    public <A extends Annotation> A getAnnotation( Class<A> annotationType ) {
        return annotations == null ? null : annotationType.cast( annotations.get( annotationType ) );
    }

    public <V> void set( Class<V> type, V value ) {
        if ( values == null )
            values = new HashMap<Class<?>, Object>();

        values.put( type, value );
    }

    @SuppressWarnings("unchecked")
    @Override
    public <V> V get( Class<V> type, V defaultValue ) {
        if ( values != null ) {
            Object value = values.get( type );
            // Use a direct (Cast) to make life easier with primitives.
            return type.isInstance( value ) ? (V) value : defaultValue;
        } else {
            return null;
        }
    }
}
