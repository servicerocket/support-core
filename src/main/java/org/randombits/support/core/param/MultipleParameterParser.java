/*
 * Copyright (c) 2006, David Peterson
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of "randombits.org" nor the names of its contributors
 *       may be used to endorse or promote products derived from this software
 *       without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package org.randombits.support.core.param;

import org.randombits.utils.text.TextBuffer;
import org.randombits.utils.text.TokenIterator;

import java.util.BitSet;
import java.util.HashSet;
import java.util.Set;

/**
 * This class parses lists of strings using +/- quoted form for mixed boolean filtering.
 * It allows for the definition of 'or', 'and' and 'not' items in a list, along with grouping
 * using parentheses - '(' and ')'. The actual object created depends on the implementation of the
 * {@link ParameterInterpreter} provided.
 *
 * @author David Peterson
 */
public class MultipleParameterParser {

    private static final char OPEN_PAREN_CHAR = '(';

    private static final char CLOSE_PAREN_CHAR = ')';

    private static final char REQ_CHAR = '+';

    private static final char EXCLUDED_CHAR = '-';

    private static final char QUOTE_CHAR = '"';

    private static final char ESCAPE_CHAR = '\\';

    private static final char COMMA_CHAR = ',';

    private static final char SEMICOLON_CHAR = ';';

    private static final BitSet WHITESPACE = new BitSet();

    private static final BitSet SEPARATOR = new BitSet();

    private static final BitSet RESERVED = new BitSet();

    private static final BitSet UNQUOTED = new BitSet();

    private static final BitSet QUOTED = new BitSet();

    static {
        WHITESPACE.set( ' ' );
        WHITESPACE.set( '\t' );
        WHITESPACE.set( '\r' );
        WHITESPACE.set( '\n' );

        SEPARATOR.set( COMMA_CHAR );
        SEPARATOR.set( SEMICOLON_CHAR );

        RESERVED.set( OPEN_PAREN_CHAR );
        RESERVED.set( CLOSE_PAREN_CHAR );
        RESERVED.set( REQ_CHAR );
        RESERVED.set( EXCLUDED_CHAR );
        RESERVED.set( QUOTE_CHAR );

        UNQUOTED.set( Character.MIN_VALUE, Character.MAX_VALUE, true );
        UNQUOTED.andNot( SEPARATOR );
        UNQUOTED.andNot( RESERVED );
        UNQUOTED.set( EXCLUDED_CHAR );

        QUOTED.set( Character.MIN_VALUE, Character.MAX_VALUE, true );
        QUOTED.clear( QUOTE_CHAR );
        QUOTED.clear( ESCAPE_CHAR );
    }

    private BitSet separator;

    private BitSet unquoted;

    public MultipleParameterParser() {
        this( false );
    }

    public MultipleParameterParser( boolean whitespaceSeparator ) {
        separator = new BitSet();
        separator.or( SEPARATOR );

        unquoted = new BitSet();
        unquoted.or( UNQUOTED );

        if ( whitespaceSeparator ) {
            separator.or( WHITESPACE );
            unquoted.andNot( WHITESPACE );
        }
    }

    /**
     * Parses the provided filter value, using the specified criterion parser to
     * findObject individual criterion.
     *
     * @param inputValue The text input value to parse.
     * @param context    The parameter execution context.
     * @param creator    The interpreter.
     * @return the parsed value.
     * @throws InterpretationException if there was a problem parsing the parameter.
     */
    public <T> T parse( String inputValue, ParameterContext context, Creator<T> creator )
            throws InterpretationException {
        TokenIterator i = new TokenIterator( inputValue.trim() );
        T value = parseBooleanItems( i, context, creator );
        if ( !i.atEnd() )
            throw new InterpretationException( "Unexpected values at end: '" + i.getSequence( TokenIterator.ALL_CHARS )
                    + "'" );

        return value;
    }

    private <T> T parseBooleanItems( TokenIterator i, ParameterContext context, Creator<T> creator )
            throws InterpretationException {
        Set<T> must = new HashSet<T>();
        Set<T> should = new HashSet<T>();
        Set<T> mustNot = new HashSet<T>();
        boolean continuing = true;

        while ( continuing && !i.atEnd() ) {
            handleBooleanItem( i, context, creator, must, should, mustNot );

            // Clear any extra whitespace...
            // i.getSequence(WHITESPACE);
            continuing = i.getSequence( separator, 1 ) != null;
        }

        return creator.createGroup( must, should, mustNot, context );
    }

    private <T> void handleBooleanItem( TokenIterator i, ParameterContext context, Creator<T> creator, Set<T> must, Set<T> should,
                                        Set<T> mustNot ) throws InterpretationException {
        // clear any whitespace
        i.getSequence( WHITESPACE );

        if ( i.getToken( REQ_CHAR ) != null ) {
            must.add( parseBooleanItem( i, context, creator ) );
        } else if ( i.getToken( EXCLUDED_CHAR ) != null ) {
            mustNot.add( parseBooleanItem( i, context, creator ) );
        } else {
            should.add( parseBooleanItem( i, context, creator ) );
        }
    }

    private <T> T parseBooleanGroup( TokenIterator i, ParameterContext context, Creator<T> creator )
            throws InterpretationException {
        i.getToken( OPEN_PAREN_CHAR );
        T group = parseBooleanItems( i, context, creator );
        if ( i.getToken( CLOSE_PAREN_CHAR ) == null )
            throw new InterpretationException( "Expected '" + CLOSE_PAREN_CHAR + "'" );
        return group;
    }

    private <T> T parseBooleanItem( TokenIterator i, ParameterContext context, Creator<T> creator )
            throws InterpretationException {
        if ( i.matchToken( OPEN_PAREN_CHAR ) ) {
            return parseBooleanGroup( i, context, creator );
        } else {
            return parseInstance( i, context, creator );
        }
    }

    private <T> T parseInstance( TokenIterator i, ParameterContext context, Creator<T> creator ) throws InterpretationException {
        TextBuffer buff = new TextBuffer();

        boolean done = false;

        while ( !done ) {
            if ( i.getToken( QUOTE_CHAR ) != null ) {
                readQuotedCriterion( i, buff );
            } else {
                CharSequence value = i.getSequence( unquoted, 1 );
                if ( value != null )
                    buff.add( value );
                else
                    done = true;
            }
        }
        return creator.createInstance( buff.toString(), context );
    }

    private void readQuotedCriterion( TokenIterator i, TextBuffer buff ) throws InterpretationException {
        while ( !i.atEnd() && !i.matchToken( QUOTE_CHAR ) ) {
            buff.add( i.getSequence( QUOTED ) );
            if ( i.getToken( ESCAPE_CHAR ) != null ) {
                CharSequence escaped = i.getSequence( TokenIterator.ALL_CHARS, 1, 1 );
                if ( escaped == null )
                    throw new InterpretationException( "Expected a character to escape after '" + ESCAPE_CHAR + "'" );

                buff.add( escaped );
            }
        }
        if ( i.getToken( QUOTE_CHAR ) == null )
            throw new InterpretationException( "Expected '" + QUOTE_CHAR + "'" );
    }

    /**
     * This interface is used to create groups and instances of the target type.
     *
     * @param <T> The target type.
     */
    public static interface Creator<T> {

        T createInstance( String inputValue, ParameterContext context ) throws InterpretationException;

        T createGroup( Set<? extends T> must, Set<? extends T> should, Set<? extends T> mustNot, ParameterContext context );
    }
}
