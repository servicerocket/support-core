package org.randombits.support.core.param;

/**
 * Provides a generic interface to retrieve parameter values, as well as
 * construct a custom {@link ParameterContext} based on the execution context.
 */
public interface ParameterSource {

    public String getValue( String name );

    public ParameterContext createContext();
}
