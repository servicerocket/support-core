package org.randombits.support.core.param.collection;

import org.randombits.support.core.param.Context;
import org.randombits.support.core.param.ParameterInterpreter;

import java.lang.annotation.*;
import java.util.HashSet;
import java.util.Set;

/**
 * Defines rules about how the a Collection should be interpreted.
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Context
public @interface SetValues {

    /**
     * @return the type of content the Set will contain.
     */
    Class<?> value();

    /**
     * @return the interpreter. It must support converting into the value type.
     */
    Class<? extends ParameterInterpreter<?>> interpreter();

    /**
     * The {@link java.util.regex.Pattern} string to use to split values in the set.
     *
     * @return the separator pattern.
     */
    String separator() default AbstractCollectionInterpreter.DEFAULT_SEPARATOR;

    Class<? extends Set> implementation() default HashSet.class;
}
