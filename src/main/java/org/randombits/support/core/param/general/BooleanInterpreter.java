package org.randombits.support.core.param.general;

import org.randombits.support.core.param.AbstractParameterInterpreter;
import org.randombits.support.core.param.InterpretationException;
import org.randombits.support.core.param.ParameterContext;

/**
 * Converts a String into a Boolean value.
 */
public class BooleanInterpreter extends AbstractParameterInterpreter<Boolean> {

    @Override
    public Boolean interpret( String inputValue, ParameterContext context ) throws InterpretationException {
        BooleanValues values = context.getAnnotation( BooleanValues.class );
        if ( values != null ) {
            return parseValue( inputValue, values );
        } else {
            return Boolean.parseBoolean( inputValue );
        }
    }

    private Boolean parseValue( String inputValue, BooleanValues values ) {
        for ( String trueValue : values.isTrue() ) {
            if ( equal( trueValue, inputValue, values.caseSensitive() ) )
                return Boolean.TRUE;
        }

        for ( String falseValue : values.isFalse() ) {
            if ( equal( falseValue, inputValue, values.caseSensitive() ) )
                return Boolean.FALSE;
        }
        return null;
    }

    private boolean equal( String value1, String value2, boolean caseSensitive ) {
        return caseSensitive ? value1.equalsIgnoreCase( value2 ) : value1.equals( value2 );
    }
}
