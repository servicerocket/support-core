package org.randombits.support.core.param.general;

import org.randombits.support.core.param.AbstractParameterInterpreter;
import org.randombits.support.core.param.InterpretationException;
import org.randombits.support.core.param.ParameterContext;

import java.text.NumberFormat;
import java.text.ParseException;

/**
 * Interprets the provided String as a Double. If a {@link NumberFormat} is provided
 * in the {@link ParameterContext}, it will be used to parse the string value, otherwise
 * the {@link Double#parseDouble(String)} method is used. If there are any issues
 * parsing the value, an
 */
public class DoubleInterpreter extends AbstractParameterInterpreter<Double> {

    /**
     * Interprets the provided String as a Double. If a {@link NumberFormat} is provided
     * in the {@link ParameterContext}, it will be used to parse the string value, otherwise
     * the {@link Double#parseDouble(String)} method is used. If there are any issues
     * parsing the value, an {@link InterpretationException} is thrown.
     *
     * @param context The context the value is being interpreted in.
     * @return The double value.
     * @throws InterpretationException
     */
    @Override
    public Double interpret( String inputValue, ParameterContext context )
            throws InterpretationException {
        NumberFormat format = context.get( NumberFormat.class, null );
        // First, try the Decimal format.
        if ( format != null ) {
            return parseDouble( inputValue, format );
        } else {
            // Otherwise, try a straight parse.
            try {
                return Double.parseDouble( inputValue );
            } catch ( NumberFormatException e ) {
                throw new InterpretationException( e.getMessage(), e );
            }
        }
    }

    private double parseDouble( String value, NumberFormat format ) throws InterpretationException {
        try {
            return format.parse( value ).doubleValue();
        } catch ( ParseException e ) {
            throw new InterpretationException( e.getMessage(), e );
        }
    }
}
