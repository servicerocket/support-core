package org.randombits.support.core.param.collection;

import org.randombits.support.core.param.Context;
import org.randombits.support.core.param.ParameterInterpreter;
import org.randombits.support.core.param.general.StringInterpreter;

import java.lang.annotation.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Defines rules about how the Set should be interpreted.
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Context
public @interface ListValues {

    Class<?> value() default String.class;

    Class<? extends ParameterInterpreter<?>> interpreter() default StringInterpreter.class;

    /**
     * The {@link java.util.regex.Pattern} string to use to split values in the set.
     *
     * @return
     */
    String separator() default ListInterpreter.DEFAULT_SEPARATOR;

    /**
     * The {@link List} implementation to use. Defaults to {@link ArrayList}.
     * The provided type must have a default constructor, or an exception will be thrown on construction.
     *
     * @return The list type.
     */
    Class<? extends List> listType() default ArrayList.class;
}
