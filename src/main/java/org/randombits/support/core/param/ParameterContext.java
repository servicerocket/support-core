package org.randombits.support.core.param;

import java.lang.annotation.Annotation;

/**
 * Provides context for parameter interpretation.
 */
public interface ParameterContext {

    <A extends Annotation> void setAnnotation( A annotation );

    <A extends Annotation> A getAnnotation( Class<A> annotationType );

    <V> void set( Class<V> type, V value );

    /**
     * Returns the context value of the provided type, if one exists.
     *
     * @param type         The type to find.
     * @param defaultValue The default value to return if none is present in the context.
     * @param <V>          The value type.
     * @return The context instance of the provided type, or <code>null</code>.
     */
    <V> V get( Class<V> type, V defaultValue );
}
