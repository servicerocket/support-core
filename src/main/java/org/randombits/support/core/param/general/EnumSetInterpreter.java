package org.randombits.support.core.param.general;

import org.randombits.support.core.param.InterpretationException;
import org.randombits.support.core.param.ParameterAssistant;
import org.randombits.support.core.param.ParameterContext;
import org.randombits.support.core.param.collection.AbstractCollectionInterpreter;

import java.util.Collection;
import java.util.EnumSet;

/**
 * Interprets a string parameter as an {@link EnumSet}. EnumSet Parameters <b>must</b> have a
 * {@link EnumSetValues} annotation which declares the type of Enum class the parameter returns.
 */
public class EnumSetInterpreter extends AbstractCollectionInterpreter<EnumSet> {

    public EnumSetInterpreter( ParameterAssistant parameterAssistant ) {
        super( parameterAssistant );
    }

    @Override
    protected Config<EnumSet> createConfig( ParameterContext context ) throws InterpretationException {
        EnumSetValues values = context.getAnnotation( EnumSetValues.class );

        if ( values == null )
            throw new InterpretationException( "Please provide an @EnumSetValues annotation for all EnumSet parameters" );

        return new Config<EnumSet>( values.separator(), values.value(), EnumInterpreter.class, EnumSet.class );
    }

    @Override
    protected <V> Collection<V> createInstance( Class<? extends EnumSet> collectionType, Class<V> valueType ) throws InterpretationException {
        Class<? extends Enum> enumType = (Class<? extends Enum>) valueType;
        return (Collection<V>) EnumSet.noneOf( enumType );
    }
}
