package org.randombits.support.core.param;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Marks a {@link Parameters} instance with a particular interpreter to convert the string
 * value to another Object instance.
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface Interceptors {

    /**
     * The {@link org.randombits.support.core.param.ParameterInterpreter} class to interpret with. The class
     * will be constructed and injected with any dependencies before being
     * used.
     *
     * @return The parameter interpreter class.
     */
    Class<? extends ParameterInterceptor>[] value();
}
