package org.randombits.support.core.param;

import java.lang.annotation.*;

/**
 * Marks a parameter as providing the default value if the parameter string can't be evaluated.
 */
@Target({ElementType.PARAMETER, ElementType.ANNOTATION_TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Context {

    /**
     * Classes that this context value should be added to the context with.
     * All classes must superclasses or interfaces implemented by the parameter or annotation type.
     *
     * @return
     */
    Class<?>[] as() default {};
}
