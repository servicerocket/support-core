package org.randombits.support.core.param.date;

import org.randombits.support.core.param.AbstractParameterInterpreter;
import org.randombits.support.core.param.InterpretationException;
import org.randombits.support.core.param.ParameterContext;

import java.text.DateFormat;
import java.text.ParseException;
import java.util.Date;

/**
 * Converts a String into a {@link Date}.
 */
public class DateInterpreter extends AbstractParameterInterpreter<Date> {

    @Override
    public Date interpret( String inputValue, ParameterContext context ) throws InterpretationException {
        DateFormat format = context.get( DateFormat.class, DateFormat.getDateInstance() );

        try {
            return format.parse( inputValue );
        } catch ( ParseException e ) {
            throw new InterpretationException( e.getMessage(), e );
        }
    }
}
