package org.randombits.support.core.param.general;

import org.randombits.support.core.param.AbstractParameterInterpreter;
import org.randombits.support.core.param.InterpretationException;
import org.randombits.support.core.param.ParameterContext;

import java.text.NumberFormat;
import java.text.ParseException;

/**
 * Converts a String into an Integer.
 */
public class LongInterpreter extends AbstractParameterInterpreter<Long> {

    public LongInterpreter() {
    }

    @Override
    public Long interpret( String inputValue, ParameterContext context )
            throws InterpretationException {
        NumberFormat format = context.get( NumberFormat.class, null );
        // First, try the Decimal format.
        if ( format != null )
            return parseLong( inputValue, format );

        // Otherwise, try a straight parseInt.
        try {
            return Long.parseLong( inputValue );
        } catch ( NumberFormatException e ) {
            throw new InterpretationException( e.getMessage(), e );
        }
    }

    private long parseLong( String value, NumberFormat format ) throws InterpretationException {
        try {
            return format.parse( value ).longValue();
        } catch ( ParseException e ) {
            throw new InterpretationException( e.getMessage(), e );
        }
    }
}
