package org.randombits.support.core.param;

/**
 * ParameterInterceptors are giving the opportunity to intercept a method call on a {@link Parameters} value
 * before the  {@link ParameterInterpreter} for the method is executed. It can return any object type.
 * If the Object type is compatible with the method return type, the object is returned directly, otherwise
 * it is converted into a String and passed on to the next interceptor, or if none are available, the interpreter.
 * If <code>null</code> is returned, the results are ignored and the parameter value String is left unchanged.
 */
public interface ParameterInterceptor {

    public enum Result {
        /**
         *
         */
        CONTINUE,

        /**
         * Indicates that processing should halt and return whatever value is currently in the
         * {@link ParameterContext}.
         */
        HALT
    }

    <T> Result intercept( MethodContext<T> context ) throws InterpretationException;
}
