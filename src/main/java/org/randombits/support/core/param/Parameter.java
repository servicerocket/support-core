package org.randombits.support.core.param;

import java.lang.annotation.*;

/**
 * This is used to annotate methods in a {@link Parameters} interface with alternate names
 * to retrieve the parameter value from. Each name in {@link #value()} is checked, in order
 * until a non-null value is returned. Eg:
 *
 * <pre><code>
 *     \@Parameter({"foo", "bar"})
 *     public String getFoo();
 * </code></pre>
 *
 * The above will search in 'foo' then 'bar'. If 'foo' is not null, it will be returned. Otherwise,
 * it will check 'bar' next.
 */
@Documented
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Parameter {

    String[] value();
}
