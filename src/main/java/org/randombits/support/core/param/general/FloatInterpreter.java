package org.randombits.support.core.param.general;

import org.randombits.support.core.param.AbstractParameterInterpreter;
import org.randombits.support.core.param.InterpretationException;
import org.randombits.support.core.param.ParameterContext;

import java.text.NumberFormat;
import java.text.ParseException;

/**
 * Interprets the provided String as a Float. If a {@link java.text.NumberFormat} is provided
 * in the {@link org.randombits.support.core.param.ParameterContext}, it will be used to parse the string value, otherwise
 * the {@link Float#parseFloat(String)} method is used. If there are any issues
 * parsing the value, an InterpretationException is thrown.
 */
public class FloatInterpreter extends AbstractParameterInterpreter<Float> {

    /**
     * Interprets the provided String as a Double. If a {@link java.text.NumberFormat} is provided
     * in the {@link org.randombits.support.core.param.ParameterContext}, it will be used to parse the string value, otherwise
     * the {@link Double#parseDouble(String)} method is used. If there are any issues
     * parsing the value, an {@link org.randombits.support.core.param.InterpretationException} is thrown.
     *
     * @param context The context the value is being interpreted in.
     * @return The double value.
     * @throws org.randombits.support.core.param.InterpretationException
     *
     */
    @Override
    public Float interpret( String inputValue, ParameterContext context )
            throws InterpretationException {
        NumberFormat format = context.get( NumberFormat.class, null );
        // First, try the Decimal format.
        if ( format != null ) {
            return parseFloat( inputValue, format );
        } else {
            // Otherwise, try a straight parse.
            try {
                return Float.parseFloat( inputValue );
            } catch ( NumberFormatException e ) {
                throw new InterpretationException( e.getMessage(), e );
            }
        }
    }

    private float parseFloat( String value, NumberFormat format ) throws InterpretationException {
        try {
            return format.parse( value ).floatValue();
        } catch ( ParseException e ) {
            throw new InterpretationException( e.getMessage(), e );
        }
    }
}
