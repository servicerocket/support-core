package org.randombits.support.core.param.general;

import org.randombits.support.core.param.AbstractParameterInterpreter;
import org.randombits.support.core.param.InterpretationException;
import org.randombits.support.core.param.ParameterContext;

/**
 * Returns the parameter value directly, since it's already a String.
 */
public class StringInterpreter extends AbstractParameterInterpreter<String> {

    @Override
    public String interpret( String inputValue, ParameterContext context ) throws InterpretationException {
        return inputValue;
    }
}
