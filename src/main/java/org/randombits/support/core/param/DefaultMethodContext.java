package org.randombits.support.core.param;

import java.lang.reflect.Method;

/**
 * Default implementation of {@link MethodContext}.
 */
public class DefaultMethodContext<ReturnType> implements MethodContext<ReturnType> {

    private final Object instance;

    private final Method method;

    private final Object[] methodParams;

    private final Class<ReturnType> returnType;

    private final ParameterContext parameterContext;

    private ReturnType returnValue;

    private ReturnType defaultValue;

    private String input;

    public DefaultMethodContext( Object instance, Method method, Object[] methodParams, Class<ReturnType> returnType, ParameterContext parameterContext ) {
        this.instance = instance;
        this.method = method;
        this.methodParams = methodParams;
        this.returnType = returnType;
        this.parameterContext = parameterContext;
    }

    @Override
    public Object getInstance() {
        return instance;
    }

    @Override
    public Method getMethod() {
        return method;
    }

    @Override
    public Object[] getMethodParams() {
        return methodParams;
    }

    @Override
    public Class<ReturnType> getReturnType() {
        return returnType;
    }

    @Override
    public void setReturnValue( ReturnType value ) {
        this.returnValue = value;
    }

    @Override
    public ReturnType getReturnValue() {
        return returnValue;
    }

    @Override
    public void setDefaultValue( ReturnType value ) {
        this.defaultValue = value;
    }

    @Override
    public ReturnType getDefaultValue() {
        return defaultValue;
    }

    @Override
    public String getInput() {
        return input;
    }

    @Override
    public void setInput( String value ) {
        this.input = value;
    }

    @Override
    public ParameterContext getParameterContext() {
        return parameterContext;
    }
}
