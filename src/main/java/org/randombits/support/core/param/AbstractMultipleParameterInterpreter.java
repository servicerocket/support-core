package org.randombits.support.core.param;

/**
 * Abstract base class for parameter interpreters that work with parameters that can be multiple combinations
 * of boolean-style values. Eg: "+foo, -bar".
 */
public abstract class AbstractMultipleParameterInterpreter<T> extends AbstractParameterInterpreter<T> implements MultipleParameterParser.Creator<T> {

    private final static MultipleParameterParser PARSER = new MultipleParameterParser();

    private MultipleParameterParser parser = PARSER;

    public AbstractMultipleParameterInterpreter() {
    }

    public AbstractMultipleParameterInterpreter( Class<T> type ) {
        super( type );
    }

    protected void setParser( MultipleParameterParser parser ) {
        this.parser = parser;
    }

    protected MultipleParameterParser getParser() {
        return parser;
    }

    @Override
    public T interpret( String inputValue, ParameterContext parameterContext ) throws InterpretationException {
        return parser.parse( inputValue, parameterContext, this );
    }
}
