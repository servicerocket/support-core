package org.randombits.support.core.param.general;

import org.randombits.support.core.param.AbstractParameterInterpreter;
import org.randombits.support.core.param.InterpretationException;
import org.randombits.support.core.param.ParameterContext;

import java.text.DecimalFormat;
import java.text.NumberFormat;

/**
 * Interprets a String as a {@link NumberFormat}.
 */
public class NumberFormatInterpreter extends AbstractParameterInterpreter<NumberFormat> {

    private static final String STANDARD_FORMAT = "standard";

    private static final String NUMBER_FORMAT = "number";

    private static final String INTEGER_FORMAT = "integer";

    private static final String PERCENT_FORMAT = "percent";

    private static final String CURRENCY_FORMAT = "currency";


    private NumberFormat getNumberFormat( String format ) {
        if ( STANDARD_FORMAT.equals( format ) )
            return NumberFormat.getInstance();
        else if ( CURRENCY_FORMAT.equals( format ) )
            return NumberFormat.getCurrencyInstance();
        else if ( INTEGER_FORMAT.equals( format ) )
            return NumberFormat.getIntegerInstance();
        else if ( PERCENT_FORMAT.equals( format ) )
            return NumberFormat.getPercentInstance();
        else if ( NUMBER_FORMAT.equals( format ) )
            return NumberFormat.getNumberInstance();
        else if ( format != null )
            return new DecimalFormat( format );

        return null;
    }

    @Override
    public NumberFormat interpret( String inputValue, ParameterContext context ) throws InterpretationException {
        return getNumberFormat( inputValue );
    }
}
