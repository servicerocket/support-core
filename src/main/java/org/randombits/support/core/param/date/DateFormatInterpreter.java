/*
 * Copyright (c) 2006, David Peterson
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of "randombits.org" nor the names of its contributors
 *       may be used to endorse or promote products derived from this software
 *       without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package org.randombits.support.core.param.date;

import org.randombits.support.core.param.AbstractParameterInterpreter;
import org.randombits.support.core.param.InterpretationException;
import org.randombits.support.core.param.ParameterContext;

import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.TimeZone;

/**
 * Handles creating a date format object.
 *
 * @author David Peterson
 */
public class DateFormatInterpreter extends AbstractParameterInterpreter<DateFormat> {

    public static final String LONG_FORMAT = "long";

    public static final String MEDIUM_FORMAT = "medium";

    public static final String SHORT_FORMAT = "short";

    public static final String DEFAULT_FORMAT = "default";

    private DateFormat getDateFormat( String value ) {
        if ( value == null || DEFAULT_FORMAT.equalsIgnoreCase( value ) )
            return DateFormat.getInstance();
        else if ( MEDIUM_FORMAT.equalsIgnoreCase( value ) )
            return DateFormat.getDateTimeInstance( DateFormat.MEDIUM, DateFormat.MEDIUM );
        else if ( SHORT_FORMAT.equalsIgnoreCase( value ) )
            return DateFormat.getDateTimeInstance( DateFormat.SHORT, DateFormat.SHORT );
        else if ( LONG_FORMAT.equalsIgnoreCase( value ) )
            return DateFormat.getDateTimeInstance( DateFormat.LONG, DateFormat.LONG );
        else
            return new SimpleDateFormat( value );
    }

    @Override
    public DateFormat interpret( String inputValue, ParameterContext context ) throws InterpretationException {
        DateFormat format = getDateFormat( inputValue );
        format.setNumberFormat( context.get( NumberFormat.class, format.getNumberFormat() ) );
        format.setTimeZone( context.get( TimeZone.class, format.getTimeZone() ) );
        return format;
    }
}
