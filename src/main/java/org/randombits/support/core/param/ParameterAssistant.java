package org.randombits.support.core.param;

/**
 * Created by IntelliJ IDEA.
 * User: david
 * Date: 23/01/12
 * Time: 2:01 PM
 * To change this template use File | Settings | File Templates.
 */
public interface ParameterAssistant {

    <T extends Parameters> T createParameters( Class<T> paramsType, ParameterSource source );
    
    <I extends ParameterInterpreter<?>> I findParameterInterpreter( Class<I> type );
}
