package org.randombits.support.core.param.source;

import java.util.Map;

/**
 * Provides a {@link Map} as a parameter source.
 */
public class MapParameterSource extends AbstractParameterSource {

    private final Map<String, String> map;

    public MapParameterSource( Map<String, String> map ) {
        this.map = map;
    }

    @Override
    public String getValue( String name ) {
        return map.get( name );
    }
}
