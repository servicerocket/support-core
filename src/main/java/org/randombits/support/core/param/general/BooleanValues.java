package org.randombits.support.core.param.general;

import org.randombits.support.core.param.Context;

import java.lang.annotation.*;

/**
 * This annotation is used by {@link BooleanInterpreter} to determine what
 * values to interpret as 'true' and 'false'. The default is simply 'true' and 'false',
 * but by using this annotation, a method can provide alternates such as 'yes' and 'no',
 * 'on' and 'off', etc.
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Context
public @interface BooleanValues {

    String[] isTrue();

    String[] isFalse();

    boolean caseSensitive() default false;
}
