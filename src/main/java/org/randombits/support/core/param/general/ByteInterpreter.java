package org.randombits.support.core.param.general;

import org.randombits.support.core.param.AbstractParameterInterpreter;
import org.randombits.support.core.param.InterpretationException;
import org.randombits.support.core.param.ParameterContext;

import java.text.NumberFormat;
import java.text.ParseException;

/**
 * Converts a String into a {@link Short}.
 */
public class ByteInterpreter extends AbstractParameterInterpreter<Byte> {

    public ByteInterpreter() {
    }

    @Override
    public Byte interpret( String inputValue, ParameterContext context )
            throws InterpretationException {
        NumberFormat format = context.get( NumberFormat.class, null );
        // First, try the Decimal format.
        if ( format != null )
            return parseByte( inputValue, format );

        // Otherwise, try a straight parseInt.
        try {
            return Byte.parseByte( inputValue );
        } catch ( NumberFormatException e ) {
            throw new InterpretationException( e.getMessage(), e );
        }
    }

    private byte parseByte( String value, NumberFormat format ) throws InterpretationException {
        try {
            return format.parse( value ).byteValue();
        } catch ( ParseException e ) {
            throw new InterpretationException( e.getMessage(), e );
        }
    }
}
