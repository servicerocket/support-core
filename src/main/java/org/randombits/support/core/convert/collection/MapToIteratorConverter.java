package org.randombits.support.core.convert.collection;

import org.randombits.support.core.convert.AbstractConverter;
import org.randombits.support.core.convert.ConversionCost;
import org.randombits.support.core.convert.ConversionException;

import java.util.Iterator;
import java.util.Map;

/**
 * Returns the {@link java.util.Map#entrySet()} as the iterator for a {@link Map}.
 */
public class MapToIteratorConverter extends AbstractConverter<Map<?, ?>, Iterator<?>> {

    public MapToIteratorConverter() {
        super( ConversionCost.METHOD_CALL );
    }

    @Override
    protected Iterator<?> convert( Map<?, ?> map ) throws ConversionException {
        return map.entrySet().iterator();
    }
}
