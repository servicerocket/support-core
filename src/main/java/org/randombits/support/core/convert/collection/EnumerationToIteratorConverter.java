package org.randombits.support.core.convert.collection;

import org.apache.commons.collections.IteratorUtils;
import org.randombits.support.core.convert.AbstractConverter;
import org.randombits.support.core.convert.ConversionCost;
import org.randombits.support.core.convert.ConversionException;

import java.util.Enumeration;
import java.util.Iterator;

/**
 * Converts an {@link Enumeration} to an {@link Iterator}.
 */
public class EnumerationToIteratorConverter extends AbstractConverter<Enumeration<?>, Iterator<?>> {

    public EnumerationToIteratorConverter() {
        super( ConversionCost.CREATE_SIMPLE_OBJECT );
    }

    @Override
    protected Iterator<?> convert( Enumeration<?> enumeration ) throws ConversionException {
        return IteratorUtils.asIterator( enumeration );
    }
}
