package org.randombits.support.core.convert.collection;

import org.randombits.support.core.convert.AbstractConverter;
import org.randombits.support.core.convert.ConversionCost;
import org.randombits.support.core.convert.ConversionException;

import java.util.Arrays;
import java.util.Collection;

/**
 * Converts an Object to a {@link Collection}. If the Object is an array, the returned collection
 * will contain each item of the array. Otherwise, it will simply contain the object itself.
 */
public class ArrayToCollectionConverter extends AbstractConverter<Object, Collection<?>> {

    public ArrayToCollectionConverter() {
        super( ConversionCost.CREATE_SIMPLE_OBJECT );
    }

    @Override
    public boolean canConvert( Class<?> sourceType, Class<?> targetType ) {
        return super.canConvert( sourceType, targetType ) && sourceType.isArray();
    }

    @Override
    protected Collection<?> convert( Object o ) throws ConversionException {
        if ( o.getClass().isArray() ) {
            return Arrays.asList( (Object[]) o );
        }
        return null;
    }
}
