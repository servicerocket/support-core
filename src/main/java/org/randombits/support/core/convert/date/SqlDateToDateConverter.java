package org.randombits.support.core.convert.date;

import org.randombits.support.core.convert.AbstractConverter;
import org.randombits.support.core.convert.ConversionCost;
import org.randombits.support.core.convert.ConversionException;

import java.util.Date;

/**
 * Converts a SQL {@link java.sql.Date} into a 'java.util' {@link java.util.Date}.
 */
public class SqlDateToDateConverter extends AbstractConverter<java.sql.Date, java.util.Date> {

    public SqlDateToDateConverter() {
        super( ConversionCost.CREATE_SIMPLE_OBJECT );
    }

    @Override
    protected Date convert( java.sql.Date date ) throws ConversionException {
        return new java.util.Date( date.getTime() );
    }
}
