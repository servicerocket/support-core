package org.randombits.support.core.convert.collection;

import org.apache.commons.collections.IteratorUtils;
import org.randombits.support.core.convert.AbstractConverter;
import org.randombits.support.core.convert.ConversionCost;
import org.randombits.support.core.convert.ConversionException;

import java.util.Iterator;

/**
 * Converts an Object into an {@link Iterator}. If the object is an array, each item will be
 * a separate value in the iterator. Otherwise, the object will be the only value returned by
 * the iterator.
 */
public class ArrayToIteratorConverter extends AbstractConverter<Object, Iterator<?>> {

    public ArrayToIteratorConverter() {
        super( ConversionCost.CREATE_SIMPLE_OBJECT );
    }

    @Override
    public boolean canConvert( Class<?> sourceType, Class<?> targetType ) {
        return super.canConvert( sourceType, targetType ) && sourceType.isArray();
    }

    @Override
    protected Iterator<?> convert( Object object ) throws ConversionException {
        if ( object.getClass().isArray() ) {
            return IteratorUtils.arrayIterator( (Object[]) object );
        }
        return null;
    }
}
