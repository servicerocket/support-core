package org.randombits.support.core.convert.date;

import org.randombits.support.core.convert.AbstractConverter;
import org.randombits.support.core.convert.ConversionCost;
import org.randombits.support.core.convert.ConversionException;

import java.util.Calendar;
import java.util.Date;

/**
 * Converts a {@link Calendar} into a {@link Date}.
 */
public class CalendarToDateConverter extends AbstractConverter<Calendar, Date> {

    public CalendarToDateConverter() {
        super( ConversionCost.CREATE_SIMPLE_OBJECT );
    }

    @Override
    protected Date convert( Calendar calendar ) throws ConversionException {
        return calendar.getTime();
    }
}
