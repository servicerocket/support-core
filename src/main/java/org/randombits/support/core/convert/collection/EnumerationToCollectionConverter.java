package org.randombits.support.core.convert.collection;

import org.randombits.support.core.convert.AbstractConverter;
import org.randombits.support.core.convert.ConversionCost;
import org.randombits.support.core.convert.ConversionException;

import java.util.Collection;
import java.util.Collections;
import java.util.Enumeration;

/**
 * Converts an {@link Enumeration} to a {@link Collection}.
 */
public class EnumerationToCollectionConverter extends AbstractConverter<Enumeration<?>, Collection<?>> {

    public EnumerationToCollectionConverter() {
        super( ConversionCost.CREATE_SIMPLE_OBJECT.and( ConversionCost.LOOP_SINGLE ) );
    }

    @Override
    protected Collection<?> convert( Enumeration<?> enumeration ) throws ConversionException {
        return Collections.list( enumeration );
    }
}
