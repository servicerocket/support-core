package org.randombits.support.core.convert;

/**
 * Provides a common interface for converting between two types. It makes use of {@link Converter}s
 * which are registered through the Atlassian Plugin module system.
 */
public interface ConversionAssistant {

    /**
     * Checks if it is possible to convert the provided source object
     * into the target type.
     *
     * @param source     The source object.
     * @param targetType The target type.
     * @return <code>true</code> if it is possible to perform the conversion.
     */
    boolean canConvert( Object source, Class<?> targetType );

    /**
     * Attempts to convert the provided source value into an object of the
     * specified target type.
     *
     * @param source     The source object.
     * @param targetType The target type.
     * @param <T>        The target type.
     * @return The target instance, or <code>null</code> if it could not be converted.
     */
    <T> T convert( Object source, Class<T> targetType ) throws ConversionException;
}
