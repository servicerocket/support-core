package org.randombits.support.core.convert;

/**
 * Performs a conversion between two types.
 */
public interface Converter {

    /**
     * @return The source type supported by this converter.
     */
    Class<?> getSourceType();

    /**
     * @return The target type supported by this converter.
     */
    Class<?> getTargetType();

    /**
     * Returns an estimate of the cost of using this converter. This is used as a guideline
     * to help determine which converter to use if there are more than one option. Implementations
     * should be honest about how much effort is required to perform the conversion.
     * See {@link ConversionCost} for more details and some default values for common costs.
     *
     * @return the conversion cost estimate.
     */
    ConversionCost getCost();


    /**
     * Checks if the provided source object can be converted to the specified type
     * by this converter.
     *
     * @param sourceType The source type.
     * @param targetType The target type.
     * @return <code>true</code> if it is supported.
     */
    boolean canConvert( Class<?> sourceType, Class<?> targetType );

    /**
     * Checks if the provided source object can be converted to the specified type
     * by this converter.
     *
     * @param source     The source object.
     * @param targetType The target type.
     * @return <code>true</code> if it is supported.
     */
    boolean canConvert( Object source, Class<?> targetType );

    /**
     * Attempts to convert the provided source to the target type.
     * If the target type is not supported by this converter, just return
     * <code>null</code>. A {@link ConversionException} should only be thrown if the type is
     * supported, but there was an unusual problem that occurred while converting.
     *
     * @param source     The source value.
     * @param targetType The target type.
     * @return The converted object, or <code>null</code> if the types cannot be converted.
     * @throws ConversionException if an unusual error occurred.
     */
    <T> T convert( Object source, Class<T> targetType ) throws ConversionException;
}
