package org.randombits.support.core.convert.collection;

import org.randombits.support.core.convert.AbstractConverter;
import org.randombits.support.core.convert.ConversionException;

import java.util.Iterator;

import static org.randombits.support.core.convert.ConversionCost.*;

/**
 * Converts any {@link Iterable} instance into an {@link Iterator}.
 */
public class IterableToIteratorConverter extends AbstractConverter<Iterable<?>, Iterator<?>> {

    public IterableToIteratorConverter() {
        super( METHOD_CALL );
    }

    @Override
    protected Iterator<?> convert( Iterable<?> iterable ) throws ConversionException {
        return iterable.iterator();
    }
}
