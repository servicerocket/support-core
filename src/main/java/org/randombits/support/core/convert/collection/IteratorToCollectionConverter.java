package org.randombits.support.core.convert.collection;

import org.apache.commons.collections.IteratorUtils;
import org.randombits.support.core.convert.AbstractConverter;
import org.randombits.support.core.convert.ConversionException;

import java.util.Collection;
import java.util.Iterator;

import static org.randombits.support.core.convert.ConversionCost.*;

/**
 * Converts an {@link Iterator} to a {@link Collection}.
 */
public class IteratorToCollectionConverter extends AbstractConverter<Iterator<?>, Collection<?>> {

    public IteratorToCollectionConverter() {
        super( CREATE_SIMPLE_OBJECT.and( LOOP_SINGLE ) );
    }

    @Override
    protected Collection<?> convert( Iterator<?> iterator ) throws ConversionException {
        return IteratorUtils.toList( iterator );
    }
}
