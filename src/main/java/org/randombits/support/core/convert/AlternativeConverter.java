package org.randombits.support.core.convert;

import java.util.Arrays;
import java.util.List;

/**
 * This converter takes a list of {@link Converter}s and will attempt to convert from the
 * source to the target with each in turn, until one of them returns a non-null value, at which
 * point the value will be returned. The order provided to the constructor determines
 * the order in which they are used.
 */
public class AlternativeConverter implements Converter {

    private final Class<?> sourceType;

    private final Class<?> targetType;

    private final Converter[] converters;

    private final ConversionCost cost;

    public AlternativeConverter( Class<?> sourceType, Class<?> targetType, Converter... converters ) {
        this.sourceType = sourceType;
        this.targetType = targetType;
        this.converters = Arrays.copyOf( converters, converters.length );

        long totalCost = 0;
        for ( Converter converter : converters )
            totalCost += converter.getCost().getValue();

        cost = ConversionCost.withValue( (int) ( totalCost / converters.length ) );
    }

    public AlternativeConverter( Class<?> sourceType, Class<?> targetType, List<Converter> converters ) {
        this( sourceType, targetType, converters.toArray( new Converter[converters.size()] ) );
    }

    @Override
    public Class<?> getSourceType() {
        return sourceType;
    }

    @Override
    public Class<?> getTargetType() {
        return targetType;
    }

    /**
     * Returns the average cost of all converters in the list.
     *
     * @return The average cost.
     */
    @Override
    public ConversionCost getCost() {
        return cost;
    }

    @Override
    public boolean canConvert( Class<?> sourceType, Class<?> targetType ) {
        for ( Converter converter : converters ) {
            if ( converter.canConvert( sourceType, targetType ) )
                return true;
        }
        return false;
    }
    
    @Override
    public boolean canConvert( Object source, Class<?> targetType ) {
        for ( Converter converter : converters ) {
            if ( converter.canConvert( source, targetType ) )
                return true;
        }
        return false;
    }

    @Override
    public <T> T convert( Object source, Class<T> targetType ) throws ConversionException {
        for ( Converter converter : converters ) {
            T value = converter.convert( source, targetType );
            if ( value != null )
                return value;
        }
        return null;
    }

    @Override
    public String toString() {
        return "AlternativeConverter{" +
                "converters=" + ( converters == null ? null : Arrays.asList( converters ) ) +
                ", cost=" + cost +
                '}';
    }
}
