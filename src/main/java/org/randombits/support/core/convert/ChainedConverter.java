package org.randombits.support.core.convert;

import java.util.Arrays;

/**
 * Creates a chain of converters, which will convert from one to the next to get the final result.
 */
public class ChainedConverter implements Converter {

    private final Converter[] converters;

    private final Class<?> sourceType;

    private final Class<?> targetType;

    private final ConversionCost cost;

    /**
     * Constructs a new Converter which will convert the source object via each sub-converter
     * in order. If any of the converters has a source type that is incompatible with the
     * target type of the previous converter. The source type for this converter will be the
     * source type of the first sub-converter, and the target will be that of the last sub-converter.
     *
     * @param converters The list of converters.
     * @throws IllegalArgumentException if there are no converters provided,
     *                                  or if the target type of one converter does not match the source type of the next..
     */
    public ChainedConverter( Converter... converters ) {
        if ( converters.length == 0 )
            throw new IllegalArgumentException( "At least one converter must be provided." );

        this.converters = converters;

        sourceType = converters[0].getSourceType();

        int sum = 0;
        Class<?> lastTarget = sourceType;
        for ( Converter converter : converters ) {
            Class<?> sourceType = converter.getSourceType();
            if ( sourceType != null && !sourceType.isAssignableFrom( lastTarget ) )
                throw new IllegalArgumentException( "The " + converter.getClass().getName() + " converter has a source type of "
                        + sourceType + " but the previous converter has a target type of " + lastTarget );
            sum += converter.getCost().getValue();
            lastTarget = converter.getTargetType();
        }

        targetType = lastTarget;
        cost = ConversionCost.withValue( sum );
    }

    /**
     * @return the source type of the first converter in the chain.
     */
    @Override
    public Class<?> getSourceType() {
        return sourceType;
    }

    /**
     * @return the target type of the last converter in the chain.
     */
    @Override
    public Class<?> getTargetType() {
        return targetType;
    }

    /**
     * @return The sum total of conversion costs for all converters in the chain.
     */
    @Override
    public ConversionCost getCost() {
        return cost;
    }

    @Override
    public boolean canConvert( Class<?> sourceType, Class<?> targetType ) {
        if ( this.sourceType.isAssignableFrom( sourceType )
                && targetType.isAssignableFrom( this.targetType ) ) {
            for ( Converter converter : converters ) {
                if ( converter.canConvert( sourceType, converter.getTargetType() ) )
                    sourceType = converter.getTargetType();
                else
                    return false;
            }
            return true;
        }
        return false;
    }

    /**
     * Checks if this converter can convert from the source to the target type.
     *
     * @param source     The source object.
     * @param targetType The target type.
     * @return <code>true</code> if the source can be converted.
     */
    @Override
    public boolean canConvert( Object source, Class<?> targetType ) {
        return source != null && converters[0].canConvert( source, converters[0].getTargetType() )
                && canConvert( source.getClass(), targetType );
    }

    @Override
    public <T> T convert( Object source, Class<T> targetType ) throws ConversionException {
        if ( targetType.isAssignableFrom( this.targetType ) ) {
            for ( Converter converter : converters ) {
                source = converter.convert( source, converter.getTargetType() );
            }
            if ( targetType.isInstance( source ) )
                return targetType.cast( source );
        }

        return null;
    }

    @Override
    public String toString() {
        return "ChainedConverter{" +
                "sourceType=" + sourceType +
                ", targetType=" + targetType +
                ", converters=" + Arrays.asList( converters ) +
                ", cost=" + cost.getValue() +
                '}';
    }
}
