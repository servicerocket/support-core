package org.randombits.support.core.convert;

/**
 * Thrown if there is an unusual error that occurs while converting.
 */
public class ConversionException extends Exception {

    public ConversionException() {
    }

    public ConversionException( String s ) {
        super( s );
    }

    public ConversionException( String s, Throwable throwable ) {
        super( s, throwable );
    }

    public ConversionException( Throwable throwable ) {
        super( throwable );
    }
}
