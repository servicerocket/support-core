package org.randombits.support.core.convert.date;

import org.randombits.support.core.convert.AbstractConverter;
import org.randombits.support.core.convert.ConversionCost;
import org.randombits.support.core.convert.ConversionException;

import java.util.Calendar;
import java.util.Date;

/**
 * Converts a {@link Date} into a {@link Calendar}.
 */
public class DateToCalendarConverter extends AbstractConverter<Date, Calendar> {

    public DateToCalendarConverter() {
        super( ConversionCost.CREATE_COMPLEX_OBJECT );
    }

    @Override
    protected Calendar convert( Date date ) throws ConversionException {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime( date );
        return calendar;
    }
}
