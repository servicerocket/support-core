package org.randombits.support.core.convert.atlassian;

import com.atlassian.sal.api.user.UserManager;
import com.atlassian.sal.api.user.UserProfile;
import org.randombits.support.core.convert.AbstractConverter;
import org.randombits.support.core.convert.ConversionCost;
import org.randombits.support.core.convert.ConversionException;

/**
 * Converts a String into an SAL {@link UserProfile}.
 */
public class StringToUserProfileConverter extends AbstractConverter<String, UserProfile> {

    private final UserManager userManager;

    public StringToUserProfileConverter( UserManager userManager ) {
        super( ConversionCost.DATABASE_LOOKUP );
        this.userManager = userManager;
    }

    @Override
    protected UserProfile convert( String username ) throws ConversionException {
        return userManager.getUserProfile( username );
    }
}
