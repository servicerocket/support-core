package org.randombits.support.core.convert;

/**
 * Helps a {@link Converter} indicate the likely cost of the conversion. The higher the cost,
 * the more memory/processing/etc the conversion will require.
 * <p/>
 * Costs can be created via the {@link #withValue(int)} method, or you can use the default values
 * provided by this class, such as {@link #METHOD_CALL}. You can also combine the default values
 * using the {@link #and(ConversionCost)} method. Eg:
 * <p/>
 * <pre><code>
 *     ConversionCost.DATABASE_LOOKUP.and(ConversionCost.CREATE_COMPLEX_OBJECT)
 * </code></pre>
 * <p/>
 * Implementations should be honest about the cost. The cost only affects if a conversion is executed
 * if there are more than one option available, in which case the cheapest will be picked first,
 * then subsequent converters will be checked until a non-null result is returned.
 */
public class ConversionCost implements Comparable<ConversionCost> {

    public static ConversionCost CAST = withValue( 1 );

    public static ConversionCost METHOD_CALL = withValue( 2 );

    public static ConversionCost CREATE_SIMPLE_OBJECT = withValue( 5 );

    public static ConversionCost CREATE_COMPLEX_OBJECT = withValue( 10 );

    public static ConversionCost LOOP_SINGLE = withValue( 10 );

    public static ConversionCost LOOP_RECURSIVE = withValue( 20 );

    public static ConversionCost SERIALIZATION = withValue( 15 );

    public static ConversionCost DATABASE_LOOKUP = withValue( 15 );

    public static ConversionCost REMOTE_ACCESS = withValue( 25 );

    public static ConversionCost FAIL = withValue( Integer.MAX_VALUE );

    public static ConversionCost withValue( int value ) {
        return new ConversionCost( value );
    }

    private final int value;

    private ConversionCost( int value ) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    /**
     * Returns a new instance with the value of the provided cost added to the local value.
     *
     * @param cost The cost to add.
     * @return The new instance, with the combined value.
     */
    public ConversionCost and( ConversionCost cost ) {
        return withValue( value + Math.min( cost.getValue(), Integer.MAX_VALUE - value ) );
    }

    /**
     * Multiplies the conversion cost by the provided multiplier value and returns a new ConversionCost instance.
     *
     * @param multiplier the value to multiply the current cost by.
     * @return The new conversion cost.
     */
    public ConversionCost times( int multiplier ) {
        return withValue( value * multiplier );
    }

    /**
     * @param otherCost The other cost value to compare to.
     * @return <code>true</code> if the current cost is cheaper than the other cost value, or the other value is
     *         <code>null</code>.
     */
    public boolean isCheaperThan( ConversionCost otherCost ) {
        return otherCost == null || value < otherCost.getValue();
    }

    @Override
    public int compareTo( ConversionCost conversionCost ) {
        return this.value - conversionCost.value;
    }

    @Override
    public boolean equals( Object o ) {
        if ( this == o ) return true;
        if ( o == null || getClass() != o.getClass() ) return false;

        ConversionCost that = (ConversionCost) o;

        if ( value != that.value ) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return value;
    }

    @Override
    public String toString() {
        return "ConversionCost{" +
                "value=" + value +
                '}';
    }
}
