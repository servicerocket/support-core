package org.randombits.support.core.convert;

import org.randombits.utils.lang.ClassUtils;

import java.util.List;

/**
 * An abstract base class for {@link Converter} instances from the {@link Source} type to the {@link Target} type.
 * Provides support for determining the {@link Source} and {@link Target} types automatically.
 */
public abstract class AbstractConverter<Source, Target> implements Converter {

    private final Class<Source> sourceType;

    private final Class<Target> targetType;

    private final ConversionCost cost;

    @SuppressWarnings({"unchecked"})
    public AbstractConverter( ConversionCost cost ) {
        List<Class<?>> types = ClassUtils.getTypeArguments( AbstractConverter.class, getClass() );
        sourceType = (Class<Source>) types.get( 0 );
        targetType = (Class<Target>) types.get( 1 );

        if ( sourceType == null || targetType == null )
            throw new IllegalArgumentException( "The source and target types cannot be determined automatically for this class. Please provide them explicitly with the AbstractConverter( Class, Class, ConversionCost ) method." );

        this.cost = cost;
    }

    public AbstractConverter( Class<Source> sourceType, Class<Target> targetType, ConversionCost cost ) {
        this.sourceType = sourceType;
        this.targetType = targetType;
        this.cost = cost;
    }

    /**
     * @return the source type being converted from.
     */
    @Override
    public Class<Source> getSourceType() {
        return sourceType;
    }

    /**
     * @return the target type being converted to.
     */
    @Override
    public Class<Target> getTargetType() {
        return targetType;
    }

    @Override
    public boolean canConvert( Class<?> sourceType, Class<?> targetType ) {
        return this.sourceType.isAssignableFrom( sourceType ) && targetType.isAssignableFrom( this.targetType );
    }

    /**
     * By default, this simply checks if the source is an instance of the {@link #getSourceType()}
     * and that the target type is assignable from the {@link #getTargetType()}
     *
     * @param source     The source object.
     * @param targetType The target type.
     * @return <code>true</code> if the object can be converted.
     */
    @Override
    public boolean canConvert( Object source, Class<?> targetType ) {
        return source != null && canConvert( source.getClass(), targetType );
    }

    /**
     * Checks if this converter can convert the provided source and target, then calls the
     * {@link #convert(Source)} method. Subclasses should override that method, along with {@link #canConvert(Object, Class)}
     * rather than this one.
     *
     * @param source     The source value.
     * @param targetType The target type.
     * @param <T>        The target type.
     * @return The converted value.
     * @throws ConversionException if there is a serious issue while converting.
     */
    @Override
    public <T> T convert( Object source, Class<T> targetType ) throws ConversionException {
        if ( canConvert( source, targetType ) ) {
            return targetType.cast( convert( sourceType.cast( source ) ) );
        }
        return null;
    }

    /**
     * Called when the converter should convert the source to the target. The source will never be null.
     *
     * @param source The source value.
     * @return The target instance, or <code>null</code> if it is unconvertible.
     * @throws ConversionException if there is a problem during conversion.
     */
    protected abstract Target convert( Source source ) throws ConversionException;

    /**
     * @return the conversion cost estimate.
     */
    @Override
    public ConversionCost getCost() {
        return cost;
    }
}
