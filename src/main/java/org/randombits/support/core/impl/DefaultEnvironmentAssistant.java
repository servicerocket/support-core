package org.randombits.support.core.impl;

import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.event.PluginEventManager;
import com.atlassian.plugin.tracker.DefaultPluginModuleTracker;
import com.atlassian.plugin.tracker.PluginModuleTracker;
import org.randombits.support.core.env.EnvironmentAssistant;
import org.randombits.support.core.env.EnvironmentProvider;
import org.springframework.beans.factory.DisposableBean;

import java.util.*;

/**
 * Default implementation of the {@link org.randombits.support.core.env.EnvironmentAssistant}.
 */
public class DefaultEnvironmentAssistant implements EnvironmentAssistant, DisposableBean {

    private class CachingCustomizer implements PluginModuleTracker.Customizer<EnvironmentProvider, EnvironmentProviderModuleDescriptor> {

        @Override
        public EnvironmentProviderModuleDescriptor adding( EnvironmentProviderModuleDescriptor descriptor ) {
            addProvider( descriptor.getModule() );
            return descriptor;
        }

        @Override
        public void removed( EnvironmentProviderModuleDescriptor descriptor ) {
            removeProvider( descriptor.getModule() );
        }


    }

    private static final Comparator<EnvironmentProvider> WEIGHT_COMPARATOR = new Comparator<EnvironmentProvider>() {
        @Override
        public int compare( EnvironmentProvider environmentProvider1, EnvironmentProvider environmentProvider2 ) {
            return environmentProvider2.getWeight() - environmentProvider1.getWeight();
        }
    };

    private final PluginModuleTracker<EnvironmentProvider, EnvironmentProviderModuleDescriptor> providerTracker;

    private final Map<Class<?>, List<EnvironmentProvider>> typeCache;

    public DefaultEnvironmentAssistant( PluginAccessor pluginAccessor, PluginEventManager pluginEventManager ) {
        typeCache = new HashMap<Class<?>, List<EnvironmentProvider>>();

        providerTracker = new DefaultPluginModuleTracker<EnvironmentProvider, EnvironmentProviderModuleDescriptor>(
                pluginAccessor, pluginEventManager, EnvironmentProviderModuleDescriptor.class, new CachingCustomizer() );
    }

    protected void addProvider( EnvironmentProvider provider ) {
        for ( Class<?> type : provider.getSupportedTypes() ) {
            List<EnvironmentProvider> providers = getProviders( type );
            if ( providers == null ) {
                providers = new ArrayList<EnvironmentProvider>( 5 );
                typeCache.put( type, providers );
            }

            if ( !providers.contains( provider ) ) {
                providers.add( provider );
                Collections.sort( providers, WEIGHT_COMPARATOR );
            }
        }
    }

    protected void removeProvider( EnvironmentProvider provider ) {
        for ( Class<?> type : provider.getSupportedTypes() ) {
            List<EnvironmentProvider> providers = getProviders( type );
            if ( providers != null ) {
                providers.remove( provider );

                // Clean up after self.
                if ( providers.isEmpty() )
                    typeCache.remove( type );
            }
        }
    }

    protected List<EnvironmentProvider> getProviders( Class<?> type ) {
        return typeCache.get( type );
    }

    @Override
    public <T> T getValue( Class<T> type ) {
        List<EnvironmentProvider> providers = getProviders( type );
        if ( providers != null ) {
            for ( EnvironmentProvider provider : providers ) {
                T value = provider.getValue( type );
                if ( value != null )
                    return value;
            }
        }
        return null;
    }

    @Override
    public void destroy() throws Exception {
        providerTracker.close();
        for ( List<EnvironmentProvider> providers : typeCache.values() ) {
            providers.clear();
        }
        typeCache.clear();
    }
}
