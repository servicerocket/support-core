package org.randombits.support.core.impl;

import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.descriptors.AbstractModuleDescriptor;
import com.atlassian.plugin.module.ModuleFactory;
import com.atlassian.util.concurrent.NotNull;
import org.dom4j.Element;
import org.randombits.support.core.param.ParameterInterpreter;

/**
 * Allows definitions of &lt;parameter-interpreter&gt; elements.
 */
public class ParameterInterpreterModuleDescriptor extends AbstractModuleDescriptor<ParameterInterpreter<?>> {

    private ParameterInterpreter<?> interpreter;

    int weight = 0;

    public ParameterInterpreterModuleDescriptor( ModuleFactory moduleFactory ) {
        super( moduleFactory );
    }

    @Override
    public void init( @NotNull Plugin plugin, @NotNull Element element ) throws PluginParseException {
        super.init( plugin, element );
    }

    @Override
    public ParameterInterpreter<?> getModule() {
        if ( interpreter == null ) {
            interpreter = moduleFactory.createModule( moduleClassName, this );
        }
        return interpreter;
    }
}
