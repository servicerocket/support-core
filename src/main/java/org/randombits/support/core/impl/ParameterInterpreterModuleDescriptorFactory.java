package org.randombits.support.core.impl;

import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.hostcontainer.HostContainer;
import com.atlassian.plugin.module.ModuleFactory;
import com.atlassian.plugin.osgi.external.SingleModuleDescriptorFactory;

/**
 * Constructs a new {@link EnvironmentProviderModuleDescriptor}s.
 */
public class ParameterInterpreterModuleDescriptorFactory extends SingleModuleDescriptorFactory<ParameterInterpreterModuleDescriptor> {

    private final ModuleFactory moduleFactory;

    public ParameterInterpreterModuleDescriptorFactory( HostContainer hostContainer, ModuleFactory moduleFactory ) {
        super( hostContainer, "parameter-interpreter", ParameterInterpreterModuleDescriptor.class );
        this.moduleFactory = moduleFactory;
    }


    @Override
    public ModuleDescriptor getModuleDescriptor( String type ) throws PluginParseException, IllegalAccessException, InstantiationException, ClassNotFoundException {
        return hasModuleDescriptor( type ) ? new ParameterInterpreterModuleDescriptor( moduleFactory ) : null;
    }
}
