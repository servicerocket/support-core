package org.randombits.support.core.impl;

import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.hostcontainer.HostContainer;
import com.atlassian.plugin.module.ModuleFactory;
import com.atlassian.plugin.osgi.external.SingleModuleDescriptorFactory;

/**
 * Constructs a new {@link EnvironmentProviderModuleDescriptor}s.
 */
public class EnvironmentProviderModuleDescriptorFactory extends SingleModuleDescriptorFactory<EnvironmentProviderModuleDescriptor> {

    private final ModuleFactory moduleFactory;

    public EnvironmentProviderModuleDescriptorFactory( HostContainer hostContainer, ModuleFactory moduleFactory ) {
        super(hostContainer, "environment-provider", EnvironmentProviderModuleDescriptor.class);
        this.moduleFactory = moduleFactory;
    }


    @Override
    public ModuleDescriptor getModuleDescriptor(String type) throws PluginParseException, IllegalAccessException, InstantiationException, ClassNotFoundException {
        return hasModuleDescriptor(type) ? new EnvironmentProviderModuleDescriptor( moduleFactory ) : null;
    }
}
