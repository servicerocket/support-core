package org.randombits.support.core.impl;

import com.atlassian.sal.api.message.I18nResolver;
import org.randombits.support.core.text.I18NAssistant;

import java.io.Serializable;
import java.util.Collection;

/**
 * Provides a methods to get I18N text from the standard plugin i18n properties
 * files. This implementation uses SAL's {@link I18nResolver}.
 *
 * @author David Peterson
 */
public final class DefaultI18NAssistant implements I18NAssistant {

    private final I18nResolver i18nResolver;

    public DefaultI18NAssistant( I18nResolver i18nResolver ) {
        this.i18nResolver = i18nResolver;
    }

    public String getText( String key ) {
        return i18nResolver.getText( key );
    }

    public String getText( String key, Object... values ) {
        Serializable[] sValues = toSerializable( values );
        return i18nResolver.getText( key, sValues );
    }

    private Serializable[] toSerializable( Object[] values ) {
        Serializable[] sValues = new Serializable[values.length];
        for ( int i = 0; i < values.length; i++ ) {
            if ( values[i] instanceof Serializable ) {
                sValues[i] = (Serializable) values[i];
            } else if ( values[i] != null ) {
                // Try converting it to a string.
                sValues[i] = values[i].toString();
            }
        }
        return sValues;
    }

    public String getText( String key, Collection<?> values ) {
        if ( values != null )
            return i18nResolver.getText( key, toSerializable( values.toArray( new Object[values.size()] ) ) );
        else
            return i18nResolver.getText( key );
    }
}
