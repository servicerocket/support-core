package org.randombits.support.core.impl;

import com.atlassian.plugin.descriptors.AbstractModuleDescriptor;
import com.atlassian.plugin.module.ModuleFactory;
import org.randombits.support.core.convert.Converter;

/**
 * Handles {@link Converter} module descriptors.
 */
public class ConverterModuleDescriptor extends AbstractModuleDescriptor<Converter> {

    private Converter converter;

    public ConverterModuleDescriptor( ModuleFactory moduleFactory ) {
        super( moduleFactory );
    }

    @Override
    public Converter getModule() {
        if ( converter == null ) {
            converter = moduleFactory.createModule( moduleClassName, this );
        }
        return converter;
    }
}
