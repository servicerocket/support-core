package org.randombits.support.core.impl;

import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.event.PluginEventManager;
import com.atlassian.plugin.tracker.DefaultPluginModuleTracker;
import com.atlassian.plugin.tracker.PluginModuleTracker;
import org.randombits.support.core.param.*;
import org.randombits.support.core.param.collection.ListInterpreter;
import org.randombits.support.core.param.collection.SetInterpreter;
import org.randombits.support.core.param.date.DateFormatInterpreter;
import org.randombits.support.core.param.date.DateInterpreter;
import org.randombits.support.core.param.general.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.DisposableBean;

import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.*;

/**
 * Default implementation of {@link org.randombits.support.core.param.ParameterAssistant}.
 */
public class DefaultParameterAssistant implements ParameterAssistant, DisposableBean {

    private enum Primitive {
        BOOLEAN( boolean.class, Boolean.class, false ),
        CHAR( char.class, Character.class, '\u0000' ),
        BYTE( byte.class, Byte.class, 0 ),
        SHORT( short.class, Short.class, 0 ),
        INT( int.class, Integer.class, 0 ),
        LONG( long.class, Long.class, 0L ),
        FLOAT( float.class, Float.class, 0.0f ),
        DOUBLE( double.class, Double.class, 0.0d );

        private final Class<?> primitiveType;

        private final Class<?> objectType;

        private final Object defaultValue;

        private Primitive( Class<?> primitiveType, Class<?> objectType, Object defaultValue ) {
            this.primitiveType = primitiveType;
            this.objectType = objectType;
            this.defaultValue = defaultValue;
        }

        public Class<?> getPrimitiveType() {
            return primitiveType;
        }

        public Class<?> getObjectType() {
            return objectType;
        }

        public Object getDefaultValue() {
            return defaultValue;
        }

        public static Primitive forType( Class<?> type ) {
            if ( type.isPrimitive() ) {
                return forPrimitiveType( type );
            } else {
                return forObjectType( type );
            }
        }

        public static Primitive forPrimitiveType( Class<?> primitiveType ) {
            for ( Primitive primitive : values() ) {
                if ( primitive.primitiveType.equals( primitiveType ) )
                    return primitive;
            }
            return null;
        }

        public static Primitive forObjectType( Class<?> objectType ) {
            for ( Primitive primitive : values() ) {
                if ( primitive.objectType.equals( objectType ) )
                    return primitive;
            }
            return null;
        }

        /**
         * Finds the primitive type for the provided type. If it is not an Object-based
         * equivalent of a primitive, <code>null</code> is returned. If it is already
         * a primitive type, the type is returned.
         *
         * @param type the type.
         * @return The primitive type.
         */
        public static Class<?> getPrimitiveType( Class<?> type ) {
            if ( type.isPrimitive() )
                return type;

            Primitive primitive = forType( type );
            return primitive == null ? null : primitive.getPrimitiveType();
        }

        /**
         * Finds the Object-based type for the provided type. If the type is not a primitive,
         * the original type is returned.
         *
         * @param type The type to check.
         * @return The Object-based type.
         */
        public static Class<?> getObjectType( Class<?> type ) {
            if ( !type.isPrimitive() )
                return type;

            Primitive primitive = forType( type );
            return primitive == null ? type : primitive.getObjectType();
        }

        @SuppressWarnings("unchecked")
        public static <T> T getDefaultValue( Class<T> type ) {
            if (type !=null && type.isPrimitive() ) {
                // Use a direct cast here because primitives don't like Class.cast(...).
            	if(forObjectType( type ) != null)
            		return (T) forObjectType( type ).getDefaultValue();
            } 
            return null;
        }

        /**
         * Casts the provided value as the specified type, either primitive or otherwise. If the
         * target type is a primitive, the default value for that primitive is returned. If the
         * value is not compatible with target type, a class cast exception will be thrown.
         *
         * @param value the object value.
         * @param type  The target type.
         * @param <T>   The target Type.
         * @return The value, cast as {@link T}
         */
        @SuppressWarnings("unchecked")
        public static <T> T getValue( Object value, Class<T> type ) {
            if ( type.isPrimitive() && value == null ) {
                return getDefaultValue( type );
            }
            return (T) value;
        }
    }

    private static final Logger LOG = LoggerFactory.getLogger( DefaultParameterAssistant.class );

    private PluginModuleTracker<ParameterInterpreter<?>, ParameterInterpreterModuleDescriptor> interpreterTracker;

    private PluginModuleTracker<ParameterInterceptor, ParameterInterceptorModuleDescriptor> interceptorTracker;

    private final List<ParameterInterpreter<?>> defaultInterpreters = new ArrayList<ParameterInterpreter<?>>();

    private final Map<Class<? extends ParameterInterpreter<?>>, ParameterInterpreter<?>> registeredInterpreters
            = new HashMap<Class<? extends ParameterInterpreter<?>>, ParameterInterpreter<?>>();

    private final Map<Class<? extends ParameterInterceptor>, ParameterInterceptor> registeredInterceptors
            = new HashMap<Class<? extends ParameterInterceptor>, ParameterInterceptor>();

    public DefaultParameterAssistant( PluginAccessor pluginAccessor, PluginEventManager pluginEventManager ) {
        registerDefaultInterpreters();

        // Interpreter tracking
        interpreterTracker = new DefaultPluginModuleTracker<ParameterInterpreter<?>, ParameterInterpreterModuleDescriptor>( pluginAccessor, pluginEventManager, ParameterInterpreterModuleDescriptor.class,
                new PluginModuleTracker.Customizer<ParameterInterpreter<?>, ParameterInterpreterModuleDescriptor>() {
                    @Override
                    public ParameterInterpreterModuleDescriptor adding( ParameterInterpreterModuleDescriptor moduleDescriptor ) {
                        registerInterpreter( moduleDescriptor.getModule() );
                        return moduleDescriptor;
                    }

                    @Override
                    public void removed( ParameterInterpreterModuleDescriptor moduleDescriptor ) {
                        unregisterInterpreter( moduleDescriptor.getModule() );
                    }
                } );

        // Interceptor tracking
        interceptorTracker = new DefaultPluginModuleTracker<ParameterInterceptor, ParameterInterceptorModuleDescriptor>( pluginAccessor, pluginEventManager, ParameterInterceptorModuleDescriptor.class, new PluginModuleTracker.Customizer<ParameterInterceptor, ParameterInterceptorModuleDescriptor>() {
            @Override
            public ParameterInterceptorModuleDescriptor adding( ParameterInterceptorModuleDescriptor moduleDescriptor ) {
                registerInterceptor( moduleDescriptor.getModule() );
                return moduleDescriptor;
            }

            @Override
            public void removed( ParameterInterceptorModuleDescriptor moduleDescriptor ) {
                unregisterInterceptor( moduleDescriptor.getModule() );
            }
        } );
    }

    private void registerDefaultInterpreters() {
        registerDefaultInterpreter( new StringInterpreter() );

        registerDefaultInterpreter( new EnumInterpreter() );
        registerDefaultInterpreter( new EnumSetInterpreter( this ) );

        registerDefaultInterpreter( new SetInterpreter( this ) );
        registerDefaultInterpreter( new ListInterpreter( this ) );

        registerDefaultInterpreter( new BooleanInterpreter() );

        registerDefaultInterpreter( new ByteInterpreter() );
        registerDefaultInterpreter( new ShortInterpreter() );
        registerDefaultInterpreter( new IntegerInterpreter() );
        registerDefaultInterpreter( new LongInterpreter() );
        registerDefaultInterpreter( new DoubleInterpreter() );
        registerDefaultInterpreter( new NumberFormatInterpreter() );

        registerDefaultInterpreter( new DateInterpreter() );
        registerDefaultInterpreter( new DateFormatInterpreter() );
    }

    private void registerDefaultInterpreter( ParameterInterpreter<?> interpreter ) {
        defaultInterpreters.add( interpreter );
        registerInterpreter( interpreter );
    }

    private void unregisterInterceptor( ParameterInterceptor interceptor ) {
        registeredInterceptors.put( interceptor.getClass(), interceptor );
    }

    private void registerInterceptor( ParameterInterceptor interceptor ) {
        registeredInterceptors.put( interceptor.getClass(), interceptor );
    }

    private void unregisterInterpreter( ParameterInterpreter<?> interpreter ) {
        registeredInterpreters.remove( interpreter.getClass() );
    }

    private void registerInterpreter( ParameterInterpreter<?> interpreter ) {
        // Not exactly sure why we have to cast here, but it won't compile without it...
        //noinspection unchecked
        registeredInterpreters.put( (Class<? extends ParameterInterpreter<?>>) interpreter.getClass(), interpreter );
    }


    @Override
    public <T extends Parameters> T createParameters( Class<T> paramsType, ParameterSource source ) {
        if ( !paramsType.isInterface() )
            throw new IllegalArgumentException( "The parameter type must be an interface." );

        return paramsType.cast( Proxy.newProxyInstance( paramsType.getClassLoader(), new Class<?>[]{paramsType},
                new ParametersInvocationHandler( paramsType, source ) ) );
    }

    @Override
    public <I extends ParameterInterpreter<?>> I findParameterInterpreter( Class<I> type ) {
        return findRegisteredInterpreter( type );
    }


    private ParameterInterpreter<?> findInterpreter( Method method, Class<?> outputType ) throws InterpretationException {
        Interpreter interpreterAnnotation = method.getAnnotation( Interpreter.class );
        ParameterInterpreter<?> interpreter;
        if ( interpreterAnnotation != null ) {
            interpreter = findRegisteredInterpreter( interpreterAnnotation.value() );
        } else {
            interpreter = findDefaultInterpreter( outputType );
        }

        if ( interpreter == null )
            throw new InterpretationException( "Unable to find a valid interpreter for '" + method.getName() );

        // Primitives don't play nice with isAssignableFrom or general casting.
        if ( outputType.isPrimitive()
                || outputType.isAssignableFrom( interpreter.getTargetType() )
                || interpreter.getTargetType().isAssignableFrom( outputType ) ) {
            //noinspection unchecked
            return interpreter;
        } else {
            throw new InterpretationException( "The interpreter's output type of '" + interpreter.getTargetType().getName() + "' is not compatible with the method's return type of '" + outputType.getName() + "'" );
        }
    }

    private <T extends ParameterInterpreter<?>> T findRegisteredInterpreter( Class<T> interpreterType ) {
        return interpreterType.cast( registeredInterpreters.get( interpreterType ) );
    }

    private ParameterInterpreter<?> findDefaultInterpreter( Class<?> outputType ) {
        // Find the non-primitive type for the output type.
        outputType = Primitive.getObjectType( outputType );

        for ( ParameterInterpreter<?> interpreter : defaultInterpreters ) {
            if ( interpreter.getTargetType().isAssignableFrom( outputType ) ) {
                return interpreter;
            }
        }
        return null;
    }


    private class ParametersInvocationHandler implements InvocationHandler {

        private final ParameterSource source;

        private final List<Class<? extends ParameterInterceptor>> interceptors;

        private ParametersInvocationHandler( Class<? extends Parameters> paramsType, ParameterSource source ) {
            this.source = source;
            interceptors = new ArrayList<Class<? extends ParameterInterceptor>>();
            findInterceptors( interceptors, paramsType );
        }

        private void findInterceptors( List<Class<? extends ParameterInterceptor>> interceptors, Class<?> paramsType ) {
            Interceptors annotation = paramsType.getAnnotation( Interceptors.class );
            if ( annotation != null ) {
                interceptors.addAll( Arrays.asList( annotation.value() ) );
            }

            for ( Class<?> type : paramsType.getInterfaces() ) {
                if ( Parameters.class.isAssignableFrom( type ) ) {
                    findInterceptors( interceptors, type );
                }
            }
        }

        @Override
        public Object invoke( Object instance, Method method, Object[] params ) throws Throwable {
            Class<?> returnType = method.getReturnType();
            Object value = null;
            try {
                value = invoke( instance, method, params, returnType );
            } catch ( Exception e ) {
                if ( supportsException( method, e ) ) {
                    throw e;
                } else if ( supportsException( method, e.getCause() ) ) {
                    throw e.getCause();
                }

                LOG.warn( "Exception while processing an interceptor: " + e.getMessage(), e );
            }

            // Make sure we return a good value for primitive method return types.
            return Primitive.getValue( value, returnType );
        }

        private <T> T invoke( Object instance, Method method, Object[] params, Class<T> outputType ) throws InterpretationException {
            MethodContext<T> context = initContext( instance, method, params, outputType );

            T value = invokeParameter( context );

            return castValue( value, context.getDefaultValue(), outputType );
        }

        @SuppressWarnings("unchecked")
        private <T> T castValue( T value, T defaultValue, Class<T> outputType ) {
            value = value == null ? defaultValue : value;

            return Primitive.getValue( value, outputType );
        }

        private <T> T invokeParameter( MethodContext<T> context ) throws InterpretationException {
            Method method = context.getMethod();
            String[] paramNames = findParamNames( method );
            for ( String name : paramNames ) {
                String value = source.getValue( name );
                if ( value != null ) {
                    context.setInput( value );
                    if ( interceptors != null ) {
                        for ( Class<? extends ParameterInterceptor> interceptorType : interceptors ) {
                            ParameterInterceptor interceptor = registeredInterceptors.get( interceptorType );
                            if ( interceptor.intercept( context ) == ParameterInterceptor.Result.HALT ) {
                                return context.getReturnValue();
                            }
                        }
                    }

                    Class<T> returnType = context.getReturnType();
                    Class<?> objectType = Primitive.getObjectType( returnType );
                    ParameterInterpreter<?> interpreter = findInterpreter( method, returnType );
                    Object objectValue = interpreter.interpret( context.getInput(), context.getParameterContext(), objectType );
                    return Primitive.getValue( objectValue, returnType );
                }
            }
            return null;
        }

        private boolean supportsException( Method method, Throwable throwable ) {
            for ( Class<?> type : method.getExceptionTypes() ) {
                if ( type.isInstance( throwable ) )
                    return true;
            }
            return false;
        }

        @SuppressWarnings("unchecked")
        protected <T> MethodContext<T> initContext( Object instance, Method method, Object[] params, Class<T> outputType ) throws InterpretationException {
            MethodContext<T> context = createContext( instance, method, params, outputType );

            Class<?>[] paramTypes = method.getParameterTypes();
            Annotation[][] paramAnnotations = method.getParameterAnnotations();
            boolean defaultSet = false;

            // Check method parameters
            if ( params != null ) {
                for ( int i = 0; i < params.length; i++ ) {
                    Class<?> paramType = paramTypes[i];
                    Object paramValue = params[i];

                    if ( getAnnotation( paramAnnotations[i], Default.class ) != null ) {
                        if ( defaultSet )
                            throw new InterpretationException( "Only one parameter can be marked as the @Default." );

                        if ( outputType.isAssignableFrom( paramType ) ) {
                            defaultSet = true;
                            context.setDefaultValue( Primitive.getValue( paramValue, outputType ) );
                        } else {
                            throw new InterpretationException( "The @Default parameter must be compatible with " + outputType.getName() + ": " + paramTypes[i].getName() );
                        }
                    } else {
                        setContextValue( context.getParameterContext(), paramType, paramValue );
                        Context ctxAnnotation = getAnnotation( paramAnnotations[i], Context.class );
                        if ( ctxAnnotation != null ) {
                            for ( Class<?> type : ctxAnnotation.as() ) {
                                if ( type.isAssignableFrom( paramType ) ) {
                                    setContextValue( context.getParameterContext(), type, paramValue );
                                }
                            }
                        }
                    }
                }
            }

            // Check method annotations
            for ( Annotation annotation : method.getAnnotations() ) {
            	if ( annotation.annotationType().isAnnotationPresent( Context.class ) ) {
                    context.getParameterContext().setAnnotation( annotation );
                }
            }

            return context;
        }

        private <T> MethodContext<T> createContext( Object instance, Method method, Object[] params, Class<T> returnType ) {
            return new DefaultMethodContext<T>( instance, method, params, returnType, source.createContext() );
        }

        private <T> void setContextValue( ParameterContext context, Class<T> paramType, Object param ) {
            context.set( paramType, paramType.cast( param ) );
        }

        private <T extends Annotation> T getAnnotation( Annotation[] annotations, Class<T> type ) {
            for ( Annotation a : annotations ) {
                if ( type.isInstance( a ) )
                    return type.cast( a );
            }
            return null;
        }

        private String[] findParamNames( Method method ) {
            // Try the @Parameter annotation first
            Parameter param = method.getAnnotation( Parameter.class );
            if ( param != null )
                return param.value();

            // Otherwise, guess based on the method name.
            String methodName = prepareMethodName( method );
            return new String[]{methodName};
        }

        private String prepareMethodName( Method method ) {
            String methodName = method.getName();
            if ( methodName.startsWith( "get" ) && methodName.length() > 3 ) {
                methodName = methodName.substring( 3, 4 ).toLowerCase() + methodName.substring( 4 );
            } else if ( boolean.class.isAssignableFrom( method.getReturnType() ) && methodName.startsWith( "is" ) && methodName.length() > 2 ) {
                methodName = methodName.substring( 2, 3 ).toLowerCase() + methodName.substring( 3 );
            }
            return methodName;
        }

    }

    @Override
    public void destroy() throws Exception {
        registeredInterpreters.clear();
        interpreterTracker.close();
        interpreterTracker = null;

        registeredInterceptors.clear();
        interceptorTracker.close();
        interceptorTracker = null;
    }
}
