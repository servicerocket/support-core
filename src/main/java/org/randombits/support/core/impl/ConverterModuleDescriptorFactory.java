package org.randombits.support.core.impl;

import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.hostcontainer.HostContainer;
import com.atlassian.plugin.module.ModuleFactory;
import com.atlassian.plugin.osgi.external.SingleModuleDescriptorFactory;

/**
 * Constructs a new {@link org.randombits.support.core.impl.EnvironmentProviderModuleDescriptor}s.
 */
public class ConverterModuleDescriptorFactory extends SingleModuleDescriptorFactory<ConverterModuleDescriptor> {

    private final ModuleFactory moduleFactory;

    public ConverterModuleDescriptorFactory( HostContainer hostContainer, ModuleFactory moduleFactory ) {
        super( hostContainer, "converter", ConverterModuleDescriptor.class );
        this.moduleFactory = moduleFactory;
    }


    @Override
    public ModuleDescriptor getModuleDescriptor( String type ) throws PluginParseException, IllegalAccessException, InstantiationException, ClassNotFoundException {
        return hasModuleDescriptor( type ) ? new ConverterModuleDescriptor( moduleFactory ) : null;
    }
}
