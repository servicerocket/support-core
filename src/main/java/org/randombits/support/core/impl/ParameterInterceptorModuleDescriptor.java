package org.randombits.support.core.impl;

import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.descriptors.AbstractModuleDescriptor;
import com.atlassian.plugin.module.ModuleFactory;
import com.atlassian.util.concurrent.NotNull;
import org.dom4j.Element;
import org.randombits.support.core.param.ParameterInterceptor;

/**
 * Allows definitions of &lt;environment-provider&gt; elements.
 */
public class ParameterInterceptorModuleDescriptor extends AbstractModuleDescriptor<ParameterInterceptor> {

    private ParameterInterceptor interceptor;

    int weight = 0;

    public ParameterInterceptorModuleDescriptor( ModuleFactory moduleFactory ) {
        super( moduleFactory );
    }

    @Override
    public void init( @NotNull Plugin plugin, @NotNull Element element ) throws PluginParseException {
        super.init( plugin, element );
    }

    @Override
    public ParameterInterceptor getModule() {
        if ( interceptor == null ) {
            interceptor = moduleFactory.createModule( moduleClassName, this );
        }
        return interceptor;
    }
}
