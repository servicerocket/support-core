package org.randombits.support.core.impl;

import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.descriptors.AbstractModuleDescriptor;
import com.atlassian.plugin.module.ModuleFactory;
import com.atlassian.util.concurrent.NotNull;
import org.dom4j.Element;
import org.randombits.support.core.env.AbstractEnvironmentProvider;
import org.randombits.support.core.env.EnvironmentProvider;

/**
 * Allows definitions of &lt;environment-provider&gt; elements.
 */
public class EnvironmentProviderModuleDescriptor extends AbstractModuleDescriptor<EnvironmentProvider> {

    private EnvironmentProvider provider;

    int weight = 0;

    public EnvironmentProviderModuleDescriptor( ModuleFactory moduleFactory ) {
        super( moduleFactory );
    }

    @Override
    public void init( @NotNull Plugin plugin, @NotNull Element element ) throws PluginParseException {
        super.init( plugin, element );
        String weightValue = element.attributeValue( "weight" );
        if ( weightValue != null ) {
            try {
                weight = Integer.parseInt( weightValue );
            } catch ( NumberFormatException e ) {
                throw new PluginParseException( "Please provide an integer value for the weight: '" + weightValue + "'" );
            }
        }
    }

    @Override
    public EnvironmentProvider getModule() {
        if ( provider == null ) {
            provider = moduleFactory.createModule( moduleClassName, this );
            if ( weight != 0 && provider instanceof AbstractEnvironmentProvider ) {
                ( (AbstractEnvironmentProvider) provider ).setWeight( weight );
            }

        }
        return provider;
    }
}
