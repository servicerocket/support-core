package org.randombits.support.core.text;

import java.util.Collection;

/**
 * Provides a methods to get I18N text from the standard plugin i18n properties
 * files.
 *
 * @author David Peterson
 */
public interface I18NAssistant {

    String getText( String key );

    String getText( String key, Object... values );

    String getText( String key, Collection<?> values );
}
