package org.randombits.support.core.param.general;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.randombits.support.core.param.InterpretationException;
import org.randombits.support.core.param.ParameterAssistant;
import org.randombits.support.core.param.ParameterContext;

import java.util.EnumSet;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

/**
 * Tests {@link EnumSetInterpreter}.
 */
public class EnumSetInterpreterTest {

    private enum TestEnum {
        FOO, BAR
    }

    private ParameterContext parameterContext;

    private ParameterAssistant parameterAssistant;

    private EnumInterpreter enumInterpreter;

    @Before
    public void setUp() throws Exception {
        parameterContext = mock( ParameterContext.class );
        parameterAssistant = mock( ParameterAssistant.class );
        enumInterpreter = mock( EnumInterpreter.class );

        when( parameterAssistant.findParameterInterpreter( EnumInterpreter.class ) ).thenReturn( enumInterpreter );
    }

    @After
    public void tearDown() throws Exception {
        parameterContext = null;
    }

    @Test
    public void testGetTargetType() throws Exception {
        EnumSetInterpreter interpreter = new EnumSetInterpreter( parameterAssistant );
        assertSame( EnumSet.class, interpreter.getTargetType() );
    }

    @Test
    public void testInterpret() throws Exception {
        EnumSetInterpreter interpreter = new EnumSetInterpreter( parameterAssistant );

        EnumSetValues values = mock( EnumSetValues.class );
        doReturn( TestEnum.class ).when( values ).value();
        doReturn( EnumSetInterpreter.DEFAULT_SEPARATOR ).when( values ).separator();

        when( parameterContext.getAnnotation( EnumSetValues.class ) ).thenReturn( values );
        when( enumInterpreter.interpret( "foo", parameterContext, TestEnum.class ) ).thenReturn( TestEnum.FOO );
        when( enumInterpreter.interpret( "bar", parameterContext, TestEnum.class ) ).thenReturn( TestEnum.BAR );
        when( enumInterpreter.interpret( "yada", parameterContext, TestEnum.class ) ).thenThrow( new InterpretationException() );

        EnumSet<TestEnum> value;

        value = interpreter.interpret( "foo", parameterContext, EnumSet.class );
        assertEquals( EnumSet.of( TestEnum.FOO ), value );

        value = interpreter.interpret( "foo, bar", parameterContext, EnumSet.class );
        assertEquals( EnumSet.of( TestEnum.FOO, TestEnum.BAR ), value );

        try {
            value = interpreter.interpret( "foo, bar, yada", parameterContext, EnumSet.class );
            fail( "Expected InterpretationException when interpreting 'yada'" );
        } catch ( InterpretationException e ) {
            // Success!
        }
    }
}
