package org.randombits.support.core.param.general;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.randombits.support.core.param.InterpretationException;
import org.randombits.support.core.param.ParameterContext;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

/**
 * Tests {@link EnumInterpreter}.
 */
public class EnumInterpreterTest {

    private enum TestEnum {
        FOO, BAR, FOO_BAR
    }

    private ParameterContext parameterContext;

    @Before
    public void setUp() throws Exception {
        parameterContext = mock( ParameterContext.class );
    }

    @After
    public void tearDown() throws Exception {
        parameterContext = null;
    }

    @Test
    public void testGetTargetType() throws Exception {
        EnumInterpreter interpreter = new EnumInterpreter();

        assertSame( Enum.class, interpreter.getTargetType() );
    }

    @Test
    public void testInterpret() throws Exception {
        EnumInterpreter interpreter = new EnumInterpreter();

        TestEnum value;

        value = interpreter.interpret( "FOO", parameterContext, TestEnum.class );
        assertSame( TestEnum.FOO, value );

        value = interpreter.interpret( "BAR", parameterContext, TestEnum.class );
        assertSame( TestEnum.BAR, value );

        try {
            value = interpreter.interpret( "YADA", parameterContext, TestEnum.class );
            fail( "Expected an InterpretationException for 'YADA'." );
        } catch ( InterpretationException e ) {
            // Success!
        }
    }

    @Test
    public void testInterpretWithEnumValues() throws Exception {
        EnumInterpreter interpreter = new EnumInterpreter();

        EnumValues enumValues = mock( EnumValues.class );

        Alias[] aliases = new Alias[]{mockAlias( "FOO_BAR", "yada" )};

        when( enumValues.value() ).thenReturn( aliases );
        when( enumValues.toUppercase() ).thenReturn( true );
        when( parameterContext.getAnnotation( EnumValues.class ) ).thenReturn( enumValues );

        TestEnum value;

        value = interpreter.interpret( "Foo", parameterContext, TestEnum.class );
        assertSame( TestEnum.FOO, value );

        value = interpreter.interpret( "BaR", parameterContext, TestEnum.class );
        assertSame( TestEnum.BAR, value );

        value = interpreter.interpret( "YaDa", parameterContext, TestEnum.class );
        assertSame( TestEnum.FOO_BAR, value );
    }

    private Alias mockAlias( String value, String... aliases ) {
        Alias alias = mock( Alias.class );
        when( alias.value() ).thenReturn( value );
        when( alias.aliases() ).thenReturn( aliases );
        return alias;
    }

}
