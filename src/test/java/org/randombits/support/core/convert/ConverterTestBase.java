package org.randombits.support.core.convert;

import static org.mockito.Mockito.*;

/**
 * Provides utility methods for tests involving {@link Converter}s.
 */
public class ConverterTestBase {

    protected Converter mockConverter( Class<?> sourceType, Class<?> targetType ) {
        return mockConverter( sourceType, targetType, ConversionCost.withValue( 0 ) );
    }

    protected Converter mockConverter( Class<?> sourceType, Class<?> targetType, ConversionCost cost ) {
        Converter converter = mock( Converter.class, "{" + sourceType.getSimpleName() + " > " + targetType.getSimpleName() + "}" );
        doReturn( sourceType ).when( converter ).getSourceType();
        doReturn( targetType ).when( converter ).getTargetType();
        when( converter.getCost() ).thenReturn( cost );
        when( converter.canConvert( sourceType, targetType ) ).thenReturn( true );

        return converter;
    }
}
