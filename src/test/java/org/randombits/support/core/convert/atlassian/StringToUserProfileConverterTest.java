package org.randombits.support.core.convert.atlassian;

import com.atlassian.sal.api.user.UserManager;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.randombits.support.core.convert.ConversionException;

import static org.mockito.Mockito.*;

/**
 * Tests {@link StringToUserProfileConverter}.
 */
public class StringToUserProfileConverterTest {

    private UserManager userManager;

    private StringToUserProfileConverter converter;

    @Before
    public void setUp() {
        userManager = mock( UserManager.class );

        converter = new StringToUserProfileConverter( userManager );
    }

    @After
    public void tearDown() {
        userManager = null;
        converter = null;
    }

    @Test
    public void testConvert() throws ConversionException {
        converter.convert( "foo" );

        verify( userManager ).getUserProfile( "foo" );
    }
}
