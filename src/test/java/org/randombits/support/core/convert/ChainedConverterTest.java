package org.randombits.support.core.convert;

import org.junit.Test;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

/**
 * Tests {@link ChainedConverter}.
 */
public class ChainedConverterTest extends ConverterTestBase {

    private static class A {

    }

    private static class B {

    }

    private static class C {

    }

    @Test
    public void testAttributes() {
        Converter aToB = mockConverter( A.class, B.class, ConversionCost.withValue( 1 ) );
        Converter bToC = mockConverter( B.class, C.class, ConversionCost.withValue( 2 ) );

        ChainedConverter converter = new ChainedConverter( aToB, bToC );

        assertSame( A.class, converter.getSourceType() );
        assertSame( C.class, converter.getTargetType() );
        assertEquals( 3, converter.getCost().getValue() );

        verify( aToB, times( 2 ) ).getSourceType();
        verify( aToB ).getTargetType();
        verify( aToB ).getCost();

        verify( bToC ).getSourceType();
        verify( bToC ).getTargetType();
        verify( bToC ).getCost();

        verifyNoMoreInteractions( aToB, bToC );
    }

    @Test
    public void testNullConverterList() {
        try {
            ChainedConverter converter = new ChainedConverter();
            fail( "Expected an IllegalArgumentException for an empty converter chain." );
        } catch ( IllegalArgumentException e ) {
            // Expected result!
        }
    }

    @Test
    public void testIncompatibleConverters() {
        Converter aToB = mockConverter( A.class, B.class );
        Converter cToA = mockConverter( C.class, A.class );

        try {
            ChainedConverter converter = new ChainedConverter( aToB, cToA );
            fail( "Expected an IllegalArgumentException for " );
        } catch ( IllegalArgumentException e ) {
            // Expected result. Success!
            verify( aToB, times( 2 ) ).getSourceType();
            verify( aToB ).getTargetType();
            verify( aToB ).getCost();
            verify( cToA ).getSourceType();
            
            verifyNoMoreInteractions( aToB, cToA );
        }
    }
    
    @Test
    public void testConvert() throws ConversionException {
        Converter aToB = mockConverter( A.class, B.class );
        Converter bToC = mockConverter( B.class, C.class );

        ChainedConverter converter = new ChainedConverter( aToB, bToC );

        verify( aToB, times( 2 ) ).getSourceType();
        verify( aToB ).getTargetType();
        verify( aToB ).getCost();

        verify( bToC ).getSourceType();
        verify( bToC ).getTargetType();
        verify( bToC ).getCost();

        verifyNoMoreInteractions( aToB, bToC );
        
        A a = new A();
        B b = new B();
        C c = new C();
        
        when( aToB.convert( a, B.class ) ).thenReturn( b );
        when( bToC.convert( b, C.class ) ).thenReturn( c );
        
        assertSame( c, converter.convert( a, C.class ) );
        
        verify( aToB ).convert( a, B.class );
        verify( aToB, times( 2 ) ).getTargetType();
        verify( bToC ).convert( b, C.class );
        verify( bToC, times(  2 ) ).getTargetType();
        verifyNoMoreInteractions( aToB, bToC );
        
        assertNull( converter.convert( a, A.class ) );
        
        verifyNoMoreInteractions( aToB, bToC );
    }
    
    @Test
    public void testCanConvert() {
        Converter aToB = mockConverter( A.class, B.class );
        Converter bToC = mockConverter( B.class, C.class );

        ChainedConverter converter = new ChainedConverter( aToB, bToC );

        A a = new A();

        when(  aToB.canConvert( a, B.class ) ).thenReturn( true );

        assertTrue( converter.canConvert( a, C.class ) );
        assertFalse( converter.canConvert( a, B.class ) );
    }
}
