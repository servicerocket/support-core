package org.randombits.support.core.convert;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Tests {@link ConversionCost}.
 */
public class ConversionCostTest {

    @Test
    public void withValue() {
        ConversionCost cost = ConversionCost.withValue(0);
        assertEquals(0, cost.getValue());

        cost = ConversionCost.withValue(Integer.MAX_VALUE);
        assertEquals(Integer.MAX_VALUE, cost.getValue());
    }

    @Test
    public void testAnd() {
        ConversionCost cost = ConversionCost.withValue(0);
        cost = cost.and(ConversionCost.withValue(1));
        assertEquals(1, cost.getValue());
        cost = cost.and(ConversionCost.withValue(-1));
        assertEquals(0, cost.getValue());

        // Big numbers
        cost = ConversionCost.withValue(Integer.MAX_VALUE);
        cost.and(ConversionCost.withValue(1));
        assertEquals(Integer.MAX_VALUE, cost.getValue());
    }
}
