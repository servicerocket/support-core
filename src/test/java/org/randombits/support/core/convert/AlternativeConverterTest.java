package org.randombits.support.core.convert;

import org.junit.Test;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

/**
 * Tests {@link AlternativeConverter}.
 */
public class AlternativeConverterTest extends ConverterTestBase {

    private static class A {

    }

    private static class B {

    }

    private static class C extends B {

    }

    @Test
    public void testAttributes() {
        Converter aToB = mockConverter( A.class, B.class, ConversionCost.withValue( 0 ) );
        Converter aToC = mockConverter( A.class, C.class, ConversionCost.withValue( 2 ) );

        AlternativeConverter converter = new AlternativeConverter( A.class, B.class, aToB, aToC );

        assertSame( A.class, converter.getSourceType() );
        assertSame( B.class, converter.getTargetType() );
        // Cost should be the average.
        assertEquals( ConversionCost.withValue( 1 ), converter.getCost() );
    }

    @Test
    public void testConvert() throws ConversionException {
        Converter bToA = mockConverter( B.class, A.class );
        Converter cToA = mockConverter( C.class, A.class );

        AlternativeConverter converter = new AlternativeConverter( A.class, B.class, bToA, cToA );

        verify( bToA ).getCost();
        verify( cToA ).getCost();

        A a = new A();
        B b = new B();
        C c = new C();

        doReturn( a ).when( bToA ).convert( b, A.class );
        doReturn( a ).when( cToA ).convert( c, A.class );

        A result;

        // Converting from B to A will only use the first alternative.
        result = converter.convert( b, A.class );

        assertSame( a, result );

        verify( bToA ).convert( b, A.class );
        verifyNoMoreInteractions( bToA, cToA );

        // Converting from C to A will check the first, result in null and then check the second.
        result = converter.convert( c, A.class );
        
        assertSame( a, result );
        
        verify( bToA ).convert( c, A.class );
        verify( cToA ).convert( c, A.class );
        verifyNoMoreInteractions( bToA, cToA );
    }

    @Test
    public void testCanConvert() {
        Converter bToA = mockConverter( B.class, A.class );
        Converter cToA = mockConverter( C.class, A.class );

        AlternativeConverter converter = new AlternativeConverter( A.class, B.class, bToA, cToA );

        verify( bToA ).getCost();
        verify( cToA ).getCost();

        A a = new A();
        B b = new B();
        C c = new C();
        
        when( bToA.canConvert( b, A.class ) ).thenReturn( true );
        when( cToA.canConvert( c, A.class ) ).thenReturn( true );
        
        assertTrue( converter.canConvert( b, A.class ) );
        
        verify( bToA ).canConvert( b, A.class );
        verifyNoMoreInteractions( bToA, cToA );
        
        assertTrue( converter.canConvert( c, A.class ) );
        
        verify( bToA ).canConvert( c, A.class );
        verify( cToA ).canConvert( c, A.class );
        verifyNoMoreInteractions( bToA, cToA );

        assertFalse( converter.canConvert( a, A.class ) );

        verify( bToA ).canConvert( a, A.class );
        verify( cToA ).canConvert( a, A.class );
        verifyNoMoreInteractions( bToA, cToA );
    }

}
