package org.randombits.support.core.convert;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Tests {@link AbstractConverter}.
 */
public class AbstractConverterTest {

    private static class A {

    }

    private static class B {

    }

    private static class TestConverter extends AbstractConverter<A, B> {

        public TestConverter() {
            super( ConversionCost.withValue( 0 ) );
        }

        @Override
        protected B convert( A a ) throws ConversionException {
            return new B();
        }
    }

    private TestConverter converter;

    @Before
    public void setUp() {
        converter = new TestConverter();
    }

    @After
    public void tearDown() {
        converter = null;
    }

    @Test
    public void testGetSourceType() {
        assertSame( A.class, converter.getSourceType() );
    }

    @Test
    public void testGetTargetType() {
        assertSame( B.class, converter.getTargetType() );
    }

    @Test
    public void testConversionCost() {
        assertEquals( ConversionCost.withValue( 0 ), converter.getCost() );
    }

    @Test
    public void testConvertSourceType() throws ConversionException {
        B value = converter.convert( new A(), B.class );
        assertNotNull( value );

        Object object = converter.convert( new A(), Object.class );
        assertTrue( object instanceof B );

        value = converter.convert( new B(), B.class );
        assertNull( value );

        String string = converter.convert( new A(), String.class );
        assertNull( string );

    }
}
