package org.randombits.support.core.convert.collection;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.randombits.support.core.convert.ConversionException;

import java.util.Collection;

import static org.junit.Assert.*;

/**
 * Tests {@link ArrayToCollectionConverter}.
 */
public class ArrayToCollectionConverterTest {

    private ArrayToCollectionConverter converter;

    @Before
    public void setUp() {
        converter = new ArrayToCollectionConverter();
    }

    @After
    public void tearDown() {
        converter = null;
    }

    @Test
    public void testConvertObjectArray() throws ConversionException {
        Object[] array = new Object[]{1, "foo", Object.class};
        Collection<?> collection = converter.convert( array );

        assertEquals( "size", array.length, collection.size() );
        for ( Object value : array ) {
            assertTrue( collection.contains( value ) );
        }
    }


    @Test
    public void testConvertNonArray() throws ConversionException {
        Collection<?> collection = converter.convert( "foo" );
        assertNull( collection );
    }

    @Test
    public void testCanConvert() {
        Object[] array = new Object[]{1, "foo", Object.class};
        assertTrue( converter.canConvert( array, Collection.class ) );
        assertTrue( converter.canConvert( array, Object.class ) );
        assertFalse( converter.canConvert( array, String.class ) );
        assertFalse( converter.canConvert( "foo", Collection.class ) );
    }
}
