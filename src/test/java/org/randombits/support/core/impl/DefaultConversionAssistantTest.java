package org.randombits.support.core.impl;

import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.event.PluginEventManager;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.randombits.support.core.convert.ConversionCost;
import org.randombits.support.core.convert.ConversionException;
import org.randombits.support.core.convert.Converter;

import static junit.framework.Assert.*;
import static org.mockito.Mockito.*;

/**
 * Tests the {@link DefaultConversionAssistant}.
 */
public class DefaultConversionAssistantTest {

    public static class A {

        final String value;

        public A() {
            this.value = null;
        }

        public A( String value ) {
            this.value = value;
        }

        @Override
        public String toString() {
            return "A{" +
                    "value='" + value + '\'' +
                    '}';
        }
    }

    public static class B {

    }

    public static class C extends B {

    }

    public static class D implements I {

    }

    public static class E extends B implements I {

    }

    public static interface I {

    }

    private DefaultConversionAssistant conversionAssistant;

    private PluginAccessor pluginAccessor;

    private PluginEventManager pluginEventManager;

    //private DefaultPluginModuleTracker pluginModuleTracker;

    @Before
    public void setUp() {
        pluginAccessor = mock( PluginAccessor.class );
        pluginEventManager = mock( PluginEventManager.class );

        conversionAssistant = new DefaultConversionAssistant( pluginAccessor, pluginEventManager );
    }

    @After
    public void tearDown() {
        pluginAccessor = null;
        pluginEventManager = null;
        conversionAssistant = null;
    }

    @Test
    public void testConvertDirect() throws Exception {
        A aValue = new A();
        B bValue = new B();
        C cValue = new C();
        D dValue = new D();

        conversionAssistant.addConverter( new StubConverter<A, B>( A.class, B.class ) );

        B returnValue;

        // A is converted, so B should be a new instance.
        returnValue = conversionAssistant.convert( aValue, B.class );
        assertNotNull( returnValue );
        assertNotSame( bValue, returnValue );

        // B is not converted, just returned as is.
        returnValue = conversionAssistant.convert( bValue, B.class );
        assertSame( bValue, returnValue );

        // Because C is a subclass of B, it should just return C unmodified.
        returnValue = conversionAssistant.convert( cValue, B.class );
        assertSame( cValue, returnValue );

        // D is not convertible, so should be null.
        returnValue = conversionAssistant.convert( dValue, B.class );
        assertNull( returnValue );
    }

    @Test
    public void testConvertSubclass() throws Exception {
        A aValue = new A();
        B bValue = new B();
        C cValue = new C();
        D dValue = new D();

        conversionAssistant.addConverter( new StubConverter<A, C>( A.class, C.class ) );

        C returnValue;

        // A is converted, so C should be a new instance.
        returnValue = conversionAssistant.convert( aValue, C.class );
        assertNotNull( returnValue );
        assertNotSame( cValue, returnValue );

        // B is not supported, so return null.
        returnValue = conversionAssistant.convert( bValue, C.class );
        assertNull( returnValue );

        // Because C is the target so it should be the same instance
        returnValue = conversionAssistant.convert( cValue, C.class );
        assertSame( cValue, returnValue );

        // D is not convertible, so should be null.
        returnValue = conversionAssistant.convert( dValue, C.class );
        assertNull( returnValue );
    }

    @Test
    public void testConvertChained() throws Exception {
        A aValue = new A();
        B bValue = new B();
        C cValue = new C();
        D dValue = new D();

        conversionAssistant.addConverter( new StubConverter<A, C>( A.class, C.class, ConversionCost.CAST ) );
        conversionAssistant.addConverter( new StubConverter<B, D>( B.class, D.class, ConversionCost.DATABASE_LOOKUP ) );

        D returnValue;

        // A is converted via A > C; B > D, so return value should be a new instance.
        returnValue = conversionAssistant.convert( aValue, D.class );
        assertNotNull( returnValue );

        // B is converted directly via B > D, so return value should be a new instance.
        returnValue = conversionAssistant.convert( bValue, D.class );
        assertNotNull( returnValue );

        // C is converted indirectly via B > D, so return value should be a new instance.
        returnValue = conversionAssistant.convert( cValue, D.class );
        assertNotNull( returnValue );

        // D is the target, so it should return the same instance.
        returnValue = conversionAssistant.convert( dValue, D.class );
        assertSame( dValue, returnValue );
    }

    @Test
    public void testConvertInterface() throws Exception {
        A aValue = new A();
        B bValue = new B();
        C cValue = new C();
        D dValue = new D();

        conversionAssistant.addConverter( new StubConverter<A, D>( A.class, D.class ) );
        conversionAssistant.addConverter( new StubConverter<B, E>( B.class, E.class ) );

        I returnValue;

        // A is converted, so C should be a new instance.
        returnValue = conversionAssistant.convert( aValue, I.class );
        assertNotNull( returnValue );
        assertTrue( returnValue instanceof D );

        // B is not supported, so return null.
        returnValue = conversionAssistant.convert( bValue, I.class );
        assertNotNull( returnValue );
        assertTrue( returnValue instanceof E );

        // Because C is the target so it should be the same instance
        returnValue = conversionAssistant.convert( cValue, I.class );
        assertNotNull( returnValue );
        assertTrue( returnValue instanceof E );

        // D is not convertible, so should be null.
        returnValue = conversionAssistant.convert( dValue, I.class );
        assertSame( dValue, returnValue );
    }

    @Test
    public void testConvertMultipleRoutes() throws Exception {
        A aValue = new A();

        conversionAssistant.addConverter( new StubConverter<A, B>( A.class, B.class, ConversionCost.withValue( 10 ) ) );
        conversionAssistant.addConverter( new StubConverter<A, D>( A.class, D.class, ConversionCost.withValue( 1 ) ) );
        conversionAssistant.addConverter( new StubConverter<D, C>( D.class, C.class, ConversionCost.withValue( 1 ) ) );

        B returnValue;

        // Converting from A > D > C is cheaper than A > B directly
        returnValue = conversionAssistant.convert( aValue, B.class );
        assertNotNull( returnValue );
        assertEquals( C.class, returnValue.getClass() );
    }

    @Test
    public void testConvertConversionLoop() throws Exception {
        A aValue = new A();

        conversionAssistant.addConverter( new StubConverter<A, B>( A.class, B.class ) );
        conversionAssistant.addConverter( new StubConverter<B, A>( B.class, A.class ) );

        Object result;

        // Convert to A
        result = conversionAssistant.convert( aValue, A.class );
        assertSame( aValue, result );

        // Convert to B
        result = conversionAssistant.convert( aValue, B.class );
        assertTrue( result instanceof B );

        // No conversion pathway - it should just fail to convert cleanly.
        result = conversionAssistant.convert( aValue, C.class );
        assertNull( result );
    }

    @Test
    public void testConvertAlternatives() throws Exception {
        final A a1 = new A( "1" );
        final A a2 = new A( "2" );

        Converter aToB = new StubConverter<A, B>( A.class, B.class ) {
            @Override
            public boolean canConvert( Object source, Class<?> targetType ) {
                return super.canConvert( source, targetType ) && source == a1;
            }

            @Override
            public <T> T convert( Object source, Class<T> targetType ) throws ConversionException {
                if ( source == a1 )
                    return targetType.cast( new B() );
                return null;
            }
        };
        conversionAssistant.addConverter( aToB );

        Converter aToC = new StubConverter<A, B>( A.class, B.class ) {
            @Override
            public boolean canConvert( Object source, Class<?> targetType ) {
                return super.canConvert( source, targetType ) && source == a2;
            }

            @Override
            public <T> T convert( Object source, Class<T> targetType ) throws ConversionException {
                if ( source == a2 ) {
                    return targetType.cast( new C() );
                }
                return null;
            }
        };
        conversionAssistant.addConverter( aToC );

        Object result;

        result = conversionAssistant.convert( a1, B.class );
        assertTrue( result instanceof B );

        result = conversionAssistant.convert( a1, B.class );
        assertTrue( result instanceof B );

        result = conversionAssistant.convert( a1, C.class );
        assertNull( result );

        result = conversionAssistant.convert( a2, B.class );
        assertTrue( result instanceof C );

        result = conversionAssistant.convert( a2, C.class );
        assertNull( result );
    }
}
