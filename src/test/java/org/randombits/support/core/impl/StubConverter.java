package org.randombits.support.core.impl;

import org.randombits.support.core.convert.AbstractConverter;
import org.randombits.support.core.convert.ConversionCost;
import org.randombits.support.core.convert.ConversionException;

/**
 * A simple test converter which simply converts by creating a new instance of the target type.
 */
public class StubConverter<Source, Target> extends AbstractConverter<Source, Target> {

    public StubConverter( Class<Source> sourceType, Class<Target> targetType ) {
        this( sourceType, targetType, ConversionCost.CREATE_SIMPLE_OBJECT );
    }

    public StubConverter( Class<Source> sourceType, Class<Target> targetType, ConversionCost cost ) {
        super( sourceType, targetType, cost );
    }

    @Override
    public String toString() {
        return getSourceType().getSimpleName() + " > " + getTargetType().getSimpleName();
    }

    @Override
    protected Target convert( Source source ) throws ConversionException {
        try {
            return getTargetType().newInstance();
        } catch ( InstantiationException e ) {
            throw new ConversionException( e.getMessage(), e );
        } catch ( IllegalAccessException e ) {
            throw new ConversionException( e.getMessage(), e );
        }
    }
}
