package org.randombits.support.core.impl;

import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.event.PluginEventManager;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.randombits.support.core.env.EnvironmentProvider;

import java.util.*;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

/**
 * Tests the {@link DefaultEnvironmentAssistant}.
 */
public class DefaultEnvironmentAssistantTest {

    private class A {

    }

    private class B {

    }

    private class C {

    }

    private PluginAccessor pluginAccessor;

    private PluginEventManager pluginEventManager;

    private DefaultEnvironmentAssistant environmentAssistant;

    @Before
    public void setUp() {
        pluginAccessor = mock( PluginAccessor.class );
        pluginEventManager = mock( PluginEventManager.class );

        environmentAssistant = new DefaultEnvironmentAssistant( pluginAccessor, pluginEventManager );
    }

    @After
    public void tearDown() {
        pluginAccessor = null;
        pluginEventManager = null;
        environmentAssistant = null;
    }

    @Test
    public void testGetValue() {

        EnvironmentProvider provider = mock( EnvironmentProvider.class );

        when( provider.getSupportedTypes() ).thenReturn( this.<Class<?>>newSet( A.class ) );
        when( provider.getValue( A.class ) ).thenReturn( new A() );

        environmentAssistant.addProvider( provider );

        A value = environmentAssistant.getValue( A.class );

        assertNotNull( value );

        verify( provider ).getSupportedTypes();
        verify( provider ).getValue( A.class );
    }

    @Test
    public void testGetValueNoProvider() {
        EnvironmentProvider one = mockProvider( A.class );

        environmentAssistant.addProvider( one );
        hasProviders( A.class, one );
        hasProviders( B.class );
    }

    @Test
    public void testAddRemoveProvider() {
        EnvironmentProvider one = mockProvider( A.class, B.class );
        EnvironmentProvider two = mockProvider( B.class, C.class );

        hasProviders( A.class );
        hasProviders( B.class );
        hasProviders( C.class );

        environmentAssistant.addProvider( one );

        hasProviders( A.class, one );
        hasProviders( B.class, one );
        hasProviders( C.class );

        environmentAssistant.addProvider( two );

        hasProviders( A.class, one );
        hasProviders( B.class, one, two );
        hasProviders( C.class, two );

        environmentAssistant.removeProvider( one );

        hasProviders( A.class );
        hasProviders( B.class, two );
        hasProviders( C.class, two );

        environmentAssistant.removeProvider( two );

        hasProviders( A.class );
        hasProviders( B.class );
        hasProviders( C.class );
    }

    @Test
    public void testAddProviderTwice() {
        EnvironmentProvider provider = mockProvider( A.class );

        environmentAssistant.addProvider( provider );
        environmentAssistant.addProvider( provider );

        // Should just have the one instance of the provider, even though it was added twice.
        hasProviders( A.class, provider );
    }

    @Test
    public void testRemoveProviderThatIsNotRegistered() {
        EnvironmentProvider provider = mockProvider( A.class );

        environmentAssistant.removeProvider( provider );

        hasProviders( A.class );
    }

    private EnvironmentProvider mockProvider( Class<?>... types ) {
        EnvironmentProvider provider = mock( EnvironmentProvider.class );
        when( provider.getSupportedTypes() ).thenReturn( newSet( types ) );
        return provider;
    }

    private void hasProviders( Class<?> type, EnvironmentProvider... required ) {
        List<EnvironmentProvider> providers;
        providers = environmentAssistant.getProviders( type );
        assertContainsAll( providers, required );
    }

    private <T> void assertContainsAll( Collection<T> collection, T... values ) {
        if ( collection == null ) {
            assertEquals( 0, values.length );
        } else {
            assertEquals( "Collection size and values list size", collection.size(), values.length );
            for ( T value : values ) {
                assertTrue( value.toString(), collection.contains( value ) );
            }
        }
    }

    private <T> Set<T> newSet( T... values ) {
        return new HashSet<T>( Arrays.asList( values ) );
    }
}
