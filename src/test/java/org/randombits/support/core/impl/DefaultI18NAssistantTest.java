package org.randombits.support.core.impl;

import com.atlassian.sal.api.message.I18nResolver;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collection;

import static org.mockito.Mockito.*;

/**
 * Tests {@link DefaultI18NAssistant}.
 */
public class DefaultI18NAssistantTest {

    private static class Unserializable {

        private static final String UNSERIALIZABLE = "Unserializable";

        public String toString() {
            return UNSERIALIZABLE;
        }

    }

    private I18nResolver i18nResolver;

    private DefaultI18NAssistant i18NAssistant;

    @Before
    public void setUp() {
        i18nResolver = mock( I18nResolver.class );

        i18NAssistant = new DefaultI18NAssistant( i18nResolver );
    }

    @After
    public void tearDown() {
        i18nResolver = null;
        i18NAssistant = null;
    }

    @Test
    public void testGetTextSimple() {
        i18NAssistant.getText( "test" );
        verify( i18nResolver ).getText( "test" );
    }

    @Test
    public void testGetTextArray() {
        i18NAssistant.getText( "test", 1, 2, 3 );
        verify( i18nResolver ).getText( "test", 1, 2, 3 );
    }

    @Test
    public void testGetTextCollection() {
        Collection<?> values = null;

        i18NAssistant.getText( "test", values );
        verify( i18nResolver ).getText( "test" );


        values = Arrays.asList( 1, 2, 3 );
        i18NAssistant.getText( "test", values );
        verify( i18nResolver ).getText( "test", 1, 2, 3 );
    }

    @Test
    public void testGetTextUnserializable() {
        Unserializable two = new Unserializable();
        i18NAssistant.getText( "test", 1, two, 3 );
        verify( i18nResolver ).getText( "test", 1, Unserializable.UNSERIALIZABLE, 3 );
    }

    @Test
    public void testGetTextNullParam() {
        i18NAssistant.getText( "test", 1, null, 3 );
        verify( i18nResolver ).getText( "test", 1, null, 3 );
    }
}
