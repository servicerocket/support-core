package it.org.randombits.support.core;

import com.atlassian.confluence.it.User;
import com.atlassian.confluence.webdriver.AbstractWebDriverTest;
import it.com.servicerocket.randombits.AddOnTest;
import it.com.servicerocket.randombits.pageobject.AddOnOSGIPage;
import org.junit.Test;

/**
 * @author Kai Fung
 * @since 1.2.5.20140917
 */
public class SupportCoreTest extends AbstractWebDriverTest {

    @Test public void testSupportCorePluginIsInstalled() throws Exception {
        assert new AddOnTest().isInstalled(
                product.login(User.ADMIN, AddOnOSGIPage.class),
                "RB Support - Core"
        );
    }
}
